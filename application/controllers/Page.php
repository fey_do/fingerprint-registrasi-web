<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends SB_controller {

	protected $_key 	= 'id';
	protected $_class	= 'page';
	protected $layout = "layouts/main";

	function __construct()
	{
		parent::__construct();
		$this->layout = 'layouts/'.CNF_THEME.'/index';
		$this->load->model('mahasiswamodel');
		$this->model = $this->mahasiswamodel;

	}


	public function index( $page = null)
	{
		if(CNF_FRONT =='false' && $this->uri->segment(1) =='' ) :
			redirect('dashboard',301);
		endif;

		if($page != null) :
			$row = $this->db->query("SELECT * FROM tb_pages WHERE alias ='$page' and status='enable' ")->row();
			var_dump($row);die;
			if(count($row) >=1)
			{

				$this->data['pageTitle'] = $row->title;
				$this->data['pageNote'] = $row->note;
				$this->data['breadcrumb'] = 'active';

				if($row->access !='')
				{
					$access = json_decode($row->access,true)	;
				} else {
					$access = array();
				}

				// If guest not allowed
				if($row->allow_guest !=1)
				{
					$group_id = $this->session->userdata('gid');
					$isValid =  (isset($access[$group_id]) && $access[$group_id] == 1 ? 1 : 0 );
					if($isValid ==0)
					{
						redirect('',301);
					}
				}
				if($row->template =='backend')
				{
					 $this->layout = "layouts/main";
				}

				$filename = "application/views/pages/".$row->filename.".php";
				if(file_exists($filename))
				{
					$page = $row->filename;
				} else {
					redirect('',301);
				}

			} else {
				redirect('',301);
			}


		else :
			$this->data['pageTitle'] = 'Home';
			$this->data['pageNote'] = 'Welcome To Our Site';
			$this->data['breadcrumb'] = 'inactive';
			$page = 'home';
		endif;


		$this->data['content'] = $this->load->view('pages/'.$page,$this->data, true );
    	$this->load->view($this->layout, $this->data );

	}

	function  submitcontact()
	{

		$rules = array(
			array('field'   => 'name','label'   => ' Please Fill Name','rules'   => 'required'),
			array('field'   => 'email','label'   => 'email ','rules'   => 'required|email'),
			array('field'   => 'message','label'   => 'message','rules'   => 'required'),
		);


		$this->form_validation->set_rules( $rules );
		if( $this->form_validation->run() )
		{

			$data = array(
				'name'=>$this->input->post('name',true),
				'email'=>$this->input->post('email',true),
				'subject'=> 'New Form Submission',
				'notes'=>$this->input->post('message',true)
			);
			$message = $this->load->view('emails/contact', $data,true);


			$to 		= 	CNF_EMAIL;
			$subject 	= 'New Form Submission';
			$headers  	= 'MIME-Version: 1.0' . "\r\n";
			$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers 	.= 'From: '.$this->input->post('name',true).' <'.$this->input->post('sender',true).'>' . "\r\n";
				//mail($to, $subject, $message, $headers);
			$message = "Thank You , Your message has been sent !";
			$this->session->set_flashdata('message',SiteHelpers::alert('success',$message));
			redirect('contact-us',301);


		} else {
			$message = "The following errors occurred";
			$this->session->set_flashdata(array(
					'message'=>SiteHelpers::alert('error',$message),
					'errors'	=> validation_errors('<li>', '</li>')
			));
			redirect('contact-us',301);
		}
	}

	public function search(){

        $term = $this->input->get('term');

        $this->db->like('nim', $term);

        $data = $this->db->get("mahasiswa")->result(); //var_dump(json_encode($data));die;

        echo json_encode($data);
    }

	public function checkreg(){
		//if($this->access['is_detail'] ==0)
		//{
		//	SiteHelpers::alert('error','Your are not allowed to access the page');
		//	redirect('dashboard',301);
	  	//}

		$sql1 	    = $this->model->queryCheck($_GET['user_id']);
		$result1    = $this->db->query($sql1);
		$data1      = $result1->result_array($result1);


		if (intval($data1[0]['ct']) > intval($_GET['current'])) {
			$res['result'] = true;
			$res['current'] = intval($data1[0]['ct']);
		}
		else
		{
			$res['result'] = false;
		}
		echo json_encode($res);
	}

	public function registerUrl()
    {
		if (isset($_GET['user_id']) && !empty($_GET['user_id'])) {
			$user_id 	= $_GET['user_id'];
			$time_limit_reg = "15";

			echo "$user_id;SecurityKey;".$time_limit_reg.";".base_url()."page/proses_register/;".base_url()."page/getac/";
		}
    }

	public function verifikasiUrl()
	{
		if (isset($_GET['user_id']) && !empty($_GET['user_id'])) {

			$user_id 	= $_GET['user_id'];
			$finger		= $this->model->getUserFinger($user_id);
			$time_limit_ver = "15";

			echo "$user_id;".$finger[0]['finger_data'].";SecurityKey;".$time_limit_ver.";".base_url()."page/proses_verifikasi/;".base_url()."page/getac/".";extraParams";

		}

	}

	public function getac()
	{
		if (isset($_GET['vc']) && !empty($_GET['vc'])) {

		$data = $this->model->getDeviceAcSn($_GET['vc']);

		echo $data[0]['ac'].$data[0]['sn'];
		}
	}

	public function proses_register()
	{

		if (isset($_POST['RegTemp']) && !empty($_POST['RegTemp'])) {

			$data 		= explode(";",$_POST['RegTemp']);
			$vStamp 	= $data[0];
			$sn 		= $data[1];
			$user_id	= $data[2];
			$regTemp 	= $data[3];

			$device = $this->model->getDeviceBySn($sn);

			$salt = md5($device[0]['ac'].$device[0]['vkey'].$regTemp.$sn.$user_id);

			if (strtoupper($vStamp) == strtoupper($salt)) {

				$sql1 		= "SELECT MAX(finger_id) as fid FROM tb_finger_data WHERE user_id='".$user_id."'";
				$result1 	= $this->db->query($sql1);
				$datal 		= $result1->result_array();
				$fid 		= $datal[0]['fid'];

				if ($fid == 0) {
					$sql2 		= "INSERT INTO tb_finger_data SET user_id='".$user_id."', finger_id=".($fid+1).", finger_data='".$regTemp."' ";
					$result2	= $this->db->query($sql2);
					if ($result1 && $result2) {
						$res['result'] = true;
					} else {
						$res['server'] = "Terjadi kesalahan ketika memasukkan data!";
					}
				} else {
					$res['result'] = false;
					$res['user_finger_'.$user_id] = "Template sudah ada.";
				}

				echo "Daun jatuh dari angin";

			} else {

				$msg = "Parameter invalid..";

				//echo $base_path."messages.php?msg=$msg";
				echo "gagal maning";

			}


		}

	}

	public function proses_verifikasi()
	{
		if (isset($_POST['VerPas']) && !empty($_POST['VerPas'])) {

		$post 		= explode(";",$_POST['VerPas']);
		$user_id	= $post[0];
		$vStamp 	= $post[1];
		$time 		= $post[2];
		$sn 		= $post[3];

		$fingerData = $this->model->getUserFinger($user_id);
		$device 	= $this->model->getDeviceBySn($sn);

		//$sql1 		= "SELECT * FROM mahasiswa WHERE id='".$user_id."'";
		//$result1 	= $this->db->query($sql1);
		//$data 		= $result1->result_array();
		//$user_name	= $data['username'];

		$sql2 		= "SELECT MAX(id) as id FROM presensi where mahasiswa_id ='".$user_id."' and jam_presensi = 0 and tanggal_presensi = CURDATE() and waktu_selesai > CURTIME()";
		$result2 	= $this->db->query($sql2);
		$data2 		= $result2->row();
		$id_mpresensi	= $data2->id;

		//if($id_mpresensi == NULL){
		//	SiteHelpers::alert('Gagal','Anda belum melakukan permintaan presensi di aplikasi Android !');
		//} else {
		$salt = md5($sn.$fingerData[0]['finger_data'].$device[0]['vc'].$time.$user_id.$device[0]['vkey']);

			if (strtoupper($vStamp) == strtoupper($salt)) {

				$log = $this->model->createLog($user_id, $id_mpresensi, $time, $sn);

				if ($log == 1) {

					//SiteHelpers::alert('Sukses','Data anda telah tersimpan !');

				} else {

					//echo "Terjadi kesalahan saat memasukkan data";

				}

			} else {

				//$msg = "Parameter invalid..";
				//echo $base_path."messages.php?msg=$msg";
				//echo "gagal maning";

			}
		}
		//}


	}

	public function lang($lang)
	{

		$this->session->set_userdata('lang',$lang);
		 redirect($_SERVER['HTTP_REFERER']);
	}


	public function skin($skin = 'sximo')
	{

		$this->session->set_userdata('themes',$skin);
		 redirect($_SERVER['HTTP_REFERER']);
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/page.php */
