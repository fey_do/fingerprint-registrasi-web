<section class="gray-section contact" id="contact">
    <div class="container">
        <div class="row m-b-lg animated fadeInDown delayp1">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Lakukan Presensi</h1>
                <p>Masukkan NIM dan tekan tombol presensi.</p>
            </div>
        </div>

            <div class="row">
			<div class="col-md-4">

			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="user_name">Username</label>
					<select class="form-control" onchange="login_selectuser()" id='select_scan'>
						<option selected disabled="disabled"> -- Select Username -- </option>
							<?php
								$strSQL = "SELECT mahasiswa.id, mahasiswa.nama FROM mahasiswa LEFT JOIN presensi on presensi.mahasiswa_id = mahasiswa.id WHERE presensi.jam_presensi = 0 and presensi.tanggal_presensi = CURDATE() and presensi.waktu_selesai > CURTIME()";
								$query = $this->db->query($strSQL);
								$result = $query->result_array();
								foreach( $result as $row )
								{

									$value = base64_encode(site_url('page/verifikasiUrl?user_id='.$row['id']));

									echo "<option value=$value id='option' mahasiswa_id='".$row['id']."' nama='".$row['nama']."'>$row[nama]</option>";
								}
							?>
					</select>
				</div>
				<a href="" id="button_login" type="submit" onclick="" class="btn btn-success">Presensi</a>
			</div>
			<div class="col-md-4">

			</div>
		</div><!--//row-->

        <!--<div class="row">
            <div class="col-lg-12 text-center">

                <p class="m-t-sm">
                    Or follow us on social platform
                </p>
                <ul class="list-inline social-icon">
                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
        </div>-->
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg animated fadeInUp delayp1">
                <p><strong>© 2019 Politeknik Negeri Jakarta</strong><br> </p>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

	//$('title').html('Login');

	function login_selectuser(device_name, sn) {

		$("#button_login").attr("href","finspot:FingerspotVer;"+$('#select_scan').val())

	}

</script>
