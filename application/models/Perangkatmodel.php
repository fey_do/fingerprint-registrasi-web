<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Perangkatmodel extends SB_Model 
{

	public $table = 'tb_perangkat';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_perangkat.* FROM tb_perangkat   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE tb_perangkat.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
