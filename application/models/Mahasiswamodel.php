<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswamodel extends SB_Model
{

	public $table = 'mahasiswa';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();

	}

	public static function querySelect(  ){


		return "   SELECT mahasiswa.* FROM mahasiswa   ";
	}
	public static function queryWhere(  ){

		return "  WHERE mahasiswa.id IS NOT NULL   ";
	}

	public static function queryGroup(){
		return "   ";
	}

	public static function queryCheck($user_id){


		return "SELECT count(finger_id) as ct FROM tb_finger_data WHERE user_id=$user_id";
	}

	public function deviceCheckSn($sn) {

		$sql 	= "SELECT count(sn) as ct FROM demo_device WHERE sn = '".$sn."'";
		$query	= $this->db->query($sql);
		$data 	= $query->result_array();;

		if ($data['ct'] != '0' && $data['ct'] != '') {
			return "sn already exist!";
		} else {
			return 1;
		}

	}

	public function getDeviceAcSn($vc) {

		$sql 	= "SELECT * FROM tb_perangkat WHERE vc ='".$vc."'";
		$query	= $this->db->query($sql);
		$arr 	= array();
		$i 	= 0;

		if( $query->num_rows() > 0) {
		$result = $query->result_array(); //or $query->result_array() to get an array

			foreach( $result as $row )
			{
				//access columns as $row->column_name
				$arr[$i] = array(
					'device_name'	=> $row['device_name'],
					'sn'		=> $row['sn'],
					'vc'		=> $row['vc'],
					'ac'		=> $row['ac'],
					'vkey'		=> $row['vkey']
				);
				$i++;
			}
		}

		return $arr;

	}

	public function getDeviceBySn($sn) {

		$sql 	= "SELECT * FROM tb_perangkat WHERE sn ='".$sn."'";
		$query	= $this->db->query($sql);
		$arr 	= array();
		$i 	= 0;

		if( $query->num_rows() > 0) {
		$result = $query->result_array(); //or $query->result_array() to get an array
			foreach( $result as $row )
			{
				//access columns as $row->column_name
				$arr[$i] = array(
					'device_name'	=> $row['device_name'],
					'sn'		=> $row['sn'],
					'vc'		=> $row['vc'],
					'ac'		=> $row['ac'],
					'vkey'		=> $row['vkey']
				);
				$i++;
			}
		}

		return $arr;

	}

	public function getUser() {

		$sql 	= 'SELECT * FROM tb_users ORDER BY username ASC';
		$query	= $this->db->query($sql);
		$arr 	= array();
		$i 	= 0;

		if( $query->num_rows() > 0) {
		$result = $query->result_array(); //or $query->result_array() to get an array
			foreach( $result as $row )
			{

				$arr[$i] = array(
					'user_id'	=> $row['id'],
					'user_name'	=> $row['username']
				);

				$i++;

			}
		}

		return $arr;

	}

	public function checkUserName($user_name) {

		$sql	= "SELECT username FROM tb_users WHERE username = '".$user_name."'";
		$query	= $this->db->query($sql);
		$row	= $query->num_rows();

		if ($row>0) {
			return "Username exist!";
		} else {
			return "1";
		}

	}

	public function getUserFinger($user_id)
	{

		$sql 	= "SELECT * FROM tb_finger_data WHERE user_id= '".$user_id."' ";
		$query  = $this->db->query($sql);
		$arr 	= array();
		$i	= 0;

		if( $query->num_rows() > 0) {
		$result = $query->result_array(); //or $query->result_array() to get an array
			foreach( $result as $row )
			{
				$arr[$i] = array(
					'user_id'	=>$row['user_id'],
					"finger_id"	=>$row['finger_id'],
					"finger_data"	=>$row['finger_data']
					);
				$i++;
			}

		}

		return $arr;

	}

	public function createLog($user_id, $id_mpresensi, $time, $sn) {

		//$sq1 		= "INSERT INTO tb_presensi SET user_id='".$user_id."', username='".$user_name."', data='".date('Y-m-d H:i:s', strtotime($time))." (PC Time) | ".$sn." (SN)"."' ";\
		$sql = "INSERT INTO presensi SET via=0, status=1, jam_presensi='".date('H:i', strtotime($time))."', updated_at='".date('Y-m-d H:i:s', strtotime($time))."' WHERE id='".$id_mpresensi."' AND mahasiswa_id='".$user_id."'";
		$result1	= $this->db->query($sq1); 
		if ($result1) {
			return 1;
		} else {
			return "Terjadi kesalahan ketika memasukkan data presensi!";
		}

	}

	public function getLog() {

		$sql 	= 'SELECT * FROM demo_log ORDER BY log_time DESC';
		$result	= $this->db->query($sql);
		$arr 	= array();
		$i 	= 0;

		if( $query->num_rows() > 0) {
		$result = $query->result_array(); //or $query->result_array() to get an array
			foreach( $result as $row )
			{

				$arr[$i] = array(
					'log_time'		=> $row['log_time'],
					'user_name'		=> $row['user_name'],
					'data'			=> $row['data']
				);

			$i++;

			}
		}

		return $arr;

	}

	public function getDevice() {

		$sql 	= 'SELECT * FROM tb_perangkat ORDER BY device_name ASC';
		$query	= $this->db->query($sql);
		$arr 	= array();
		$i 	= 0;

		if( $query->num_rows() > 0) {
		$result = $query->result_array(); //or $query->result_array() to get an array
			foreach( $result as $row )
			{
				//access columns as $row->column_name
				$arr[$i] = array(
					'device_name'	=> $row['device_name'],
					'sn'		=> $row['sn'],
					'vc'		=> $row['vc'],
					'ac'		=> $row['ac'],
					'vkey'		=> $row['vkey']
				);
				$i++;
			}
		}

		return $arr;

	}

	public function checkFinger($user_id){

		$sql1 		= "SELECT MAX(finger_id) as fid FROM tb_finger_data WHERE id='".$user_id."'";
		$result1 	= $this->db->query($sql1);
		$datal 		= $result1->result_array();

		return $datal;

	}

}

?>
