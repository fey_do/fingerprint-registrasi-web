/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 10.1.36-MariaDB : Database - flexcode
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`flexcode` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `captcha` */

DROP TABLE IF EXISTS `captcha`;

CREATE TABLE `captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM AUTO_INCREMENT=263 DEFAULT CHARSET=latin1;

/*Data for the table `captcha` */

insert  into `captcha`(`captcha_id`,`captcha_time`,`ip_address`,`word`) values 
(262,1564478848,'::1','73308'),
(261,1564476603,'::1','31417');

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(40) DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  `session_id` varchar(40) NOT NULL,
  `user_agent` text,
  `last_activity` text,
  `user_data` text,
  PRIMARY KEY (`session_id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`id`,`ip_address`,`timestamp`,`data`,`session_id`,`user_agent`,`last_activity`,`user_data`) values 
(NULL,'::1',0,'','e29ad1db62e7ba99eae6eb351f862435','Microsoft URL Control - 6.01.9816','1564476049','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','48246324afde00c980c8e66e158121f5','Microsoft URL Control - 6.01.9816','1564476257','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','53712149aeddddd7f794c2d0dbc5afb6','Microsoft URL Control - 6.01.9816','1564475783','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','00481f92cb6bcbcb34c5706176ee5418','Microsoft URL Control - 6.01.9816','1564475999','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','b86284f4ec672d9d1b23fcda00de4646','Microsoft URL Control - 6.01.9816','1564476004','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','66dd6b2ad0eb0da7431680399dd7b33f','Microsoft URL Control - 6.01.9816','1564476042','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','d00888c08af586ccafc906e38b9a9133','Microsoft URL Control - 6.01.9816','1564475348','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','1dc8f3b9ed5e7ce4b16047c92682b3f2','Microsoft URL Control - 6.01.9816','1564475585','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','4830fefb5abcab495ea1c5623447baa4','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0','1564474951','a:8:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";s:9:\"logged_in\";b:1;s:3:\"uid\";s:1:\"1\";s:3:\"gid\";s:1:\"1\";s:3:\"eid\";s:19:\"superadmin@mail.com\";s:2:\"ll\";s:19:\"2014-09-09 03:09:05\";s:3:\"fid\";s:10:\"Root Admin\";}'),
(NULL,'::1',0,'','f808e1d95a94370c17456fb3e3f0f97e','Microsoft URL Control - 6.01.9816','1564476335','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','f3c12b4b1c113f255ce722b222f47384','Microsoft URL Control - 6.01.9816','1564477180','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','51bc2be4a922df10e0ca30d310a01931','Microsoft URL Control - 6.01.9816','1564477223','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','391c469c36f85bbd257505538afcc04f','Microsoft URL Control - 6.01.9816','1564477241','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','6b4a385bc7f43590f69752ee9470f19b','Microsoft URL Control - 6.01.9816','1564477288','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','75399fbccbb5e32cd6103bd50efcc10e','Microsoft URL Control - 6.01.9816','1564477379','a:3:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";s:17:\"flash:new:message\";a:3:{s:4:\"type\";s:5:\"Gagal\";s:7:\"caption\";s:62:\"Anda belum melakukan permintaan presensi di aplikasi Android !\";s:5:\"title\";s:5:\"Gagal\";}}'),
(NULL,'::1',0,'','c0691f862c9e30d0ceaf005235547b50','Microsoft URL Control - 6.01.9816','1564477716','a:3:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";s:17:\"flash:new:message\";N;}'),
(NULL,'::1',0,'','1e56b7e599c8f41d6c0fd92cdd1be791','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','1564478495','a:4:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";s:17:\"flash:old:message\";N;s:17:\"flash:new:message\";N;}'),
(NULL,'::1',0,'','0507497cc7936c3bb35568073922b12e','Microsoft URL Control - 6.01.9816','1564478691','a:3:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";s:17:\"flash:new:message\";N;}'),
(NULL,'::1',0,'','082af540ff7b6d1cde5d8342929c4b31','Microsoft URL Control - 6.01.9816','1564478875','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','a9fe1f1ba166464eafbe6a848a5f7612','Microsoft URL Control - 6.01.9816','1564479989','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','e9074a1f2000d214324eaee72b6feefc','Microsoft URL Control - 6.01.9816','1564480531','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','1c8af45be29e8f03b661ce13cb40aaf3','Microsoft URL Control - 6.01.9816','1564480541','a:3:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";s:17:\"flash:new:message\";a:3:{s:4:\"type\";s:5:\"Gagal\";s:7:\"caption\";s:62:\"Anda belum melakukan permintaan presensi di aplikasi Android !\";s:5:\"title\";s:5:\"Gagal\";}}'),
(NULL,'::1',0,'','38c775260f219fbdd64f5e9e8b57aaa0','Microsoft URL Control - 6.01.9816','1564475229','a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";}'),
(NULL,'::1',0,'','667a2f2792818c8dfc76fc3b5bf4683b','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0','1564480535','a:8:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";s:9:\"logged_in\";b:1;s:3:\"uid\";s:1:\"1\";s:3:\"gid\";s:1:\"1\";s:3:\"eid\";s:19:\"superadmin@mail.com\";s:2:\"ll\";s:19:\"2014-09-09 03:09:05\";s:3:\"fid\";s:10:\"Root Admin\";}'),
(NULL,'::1',0,'','8639fc353bf37d2614d0edf0efcc4759','Microsoft URL Control - 6.01.9816','1564477393','a:3:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";s:17:\"flash:new:message\";a:3:{s:4:\"type\";s:5:\"Gagal\";s:7:\"caption\";s:62:\"Anda belum melakukan permintaan presensi di aplikasi Android !\";s:5:\"title\";s:5:\"Gagal\";}}'),
(NULL,'::1',0,'','35e69c2fc3474b4a26fe19d190e2e2ae','Microsoft URL Control - 6.01.9816','1564477000','a:3:{s:9:\"user_data\";s:0:\"\";s:4:\"lang\";s:2:\"en\";s:17:\"flash:new:message\";a:3:{s:4:\"type\";s:5:\"Gagal\";s:7:\"caption\";s:63:\" Anda belum melakukan permintaan presensi di aplikasi Android !\";s:5:\"title\";s:5:\"Gagal\";}}');

/*Table structure for table `config` */

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `luas_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `config` */

insert  into `config`(`id`,`latitude`,`longitude`,`luas_area`,`created_at`,`updated_at`) values 
(1,'-6.372269','106.823018','100',NULL,'2019-07-22 15:48:32');

/*Table structure for table `faq` */

DROP TABLE IF EXISTS `faq`;

CREATE TABLE `faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` text,
  `slug` text,
  `question` text,
  `answer` text,
  `featured` tinyint(2) DEFAULT NULL,
  `published` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `faq` */

insert  into `faq`(`faq_id`,`topic`,`slug`,`question`,`answer`,`featured`,`published`) values 
(1,'Main Topic','main-topic','a:3:{i:0;s:21:\"What is Sximo Builder\";i:1;s:23:\"How to Build with Sximo\";i:2;s:17:\"Customizing Sximo\";}','a:3:{i:0;s:419:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a lacus ac massa molestie sollicitudin eu at libero. Nulla rhoncus placerat luctus. Donec et viverra eros. Nulla facilisi. Nunc malesuada dapibus dapibus. Fusce sed pretium mauris. Morbi fermentum faucibus tincidunt. Ut lectus sem, mollis eget consectetur et, porta sed elit. Praesent in congue augue. Suspendisse faucibus nisl ac arcu adipiscing lobortis.\";i:1;s:419:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a lacus ac massa molestie sollicitudin eu at libero. Nulla rhoncus placerat luctus. Donec et viverra eros. Nulla facilisi. Nunc malesuada dapibus dapibus. Fusce sed pretium mauris. Morbi fermentum faucibus tincidunt. Ut lectus sem, mollis eget consectetur et, porta sed elit. Praesent in congue augue. Suspendisse faucibus nisl ac arcu adipiscing lobortis.\";i:2;s:419:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a lacus ac massa molestie sollicitudin eu at libero. Nulla rhoncus placerat luctus. Donec et viverra eros. Nulla facilisi. Nunc malesuada dapibus dapibus. Fusce sed pretium mauris. Morbi fermentum faucibus tincidunt. Ut lectus sem, mollis eget consectetur et, porta sed elit. Praesent in congue augue. Suspendisse faucibus nisl ac arcu adipiscing lobortis.\";}',0,1),
(2,'Installation','installation','a:4:{i:0;s:18:\"System Requirement\";i:1;s:11:\"Preparation\";i:2;s:16:\"Installing Sximo\";i:3;s:21:\"Setting Configuration\";}','a:4:{i:0;s:419:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a lacus ac massa molestie sollicitudin eu at libero. Nulla rhoncus placerat luctus. Donec et viverra eros. Nulla facilisi. Nunc malesuada dapibus dapibus. Fusce sed pretium mauris. Morbi fermentum faucibus tincidunt. Ut lectus sem, mollis eget consectetur et, porta sed elit. Praesent in congue augue. Suspendisse faucibus nisl ac arcu adipiscing lobortis.\";i:1;s:419:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a lacus ac massa molestie sollicitudin eu at libero. Nulla rhoncus placerat luctus. Donec et viverra eros. Nulla facilisi. Nunc malesuada dapibus dapibus. Fusce sed pretium mauris. Morbi fermentum faucibus tincidunt. Ut lectus sem, mollis eget consectetur et, porta sed elit. Praesent in congue augue. Suspendisse faucibus nisl ac arcu adipiscing lobortis.\";i:2;s:419:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a lacus ac massa molestie sollicitudin eu at libero. Nulla rhoncus placerat luctus. Donec et viverra eros. Nulla facilisi. Nunc malesuada dapibus dapibus. Fusce sed pretium mauris. Morbi fermentum faucibus tincidunt. Ut lectus sem, mollis eget consectetur et, porta sed elit. Praesent in congue augue. Suspendisse faucibus nisl ac arcu adipiscing lobortis.\";i:3;s:419:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a lacus ac massa molestie sollicitudin eu at libero. Nulla rhoncus placerat luctus. Donec et viverra eros. Nulla facilisi. Nunc malesuada dapibus dapibus. Fusce sed pretium mauris. Morbi fermentum faucibus tincidunt. Ut lectus sem, mollis eget consectetur et, porta sed elit. Praesent in congue augue. Suspendisse faucibus nisl ac arcu adipiscing lobortis.\";}',0,1);

/*Table structure for table `gallery` */

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `image_file` text,
  `descr` text,
  `tags` text,
  `featured` tinyint(2) DEFAULT NULL,
  `published` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `gallery` */

insert  into `gallery`(`gallery_id`,`title`,`image_file`,`descr`,`tags`,`featured`,`published`) values 
(1,'APP 1','app_1.jpg','Vestibulum porta felis ut dui commodo imperdiet. Morbi volutpat nisl id metus scelerisque rhoncus.','ui',0,1),
(2,'APP 2','app_2.jpg','Vestibulum porta felis ut dui commodo imperdiet. Morbi volutpat nisl id metus scelerisque rhoncus.','ui',0,1),
(3,'APP 3','app_3.jpg','Vestibulum porta felis ut dui commodo imperdiet. Morbi volutpat nisl id metus scelerisque rhoncus.','ui',0,1),
(4,'CARD 1','card_1.jpg','Fusce nibh diam, ullamcorper et auctor sit amet, ullamcorper nec nunc. ','photo',0,1),
(5,'CARD 2','card_2.jpg','Fusce nibh diam, ullamcorper et auctor sit amet, ullamcorper nec nunc. ','photo',0,1),
(6,'CARD 3','card_3.jpg','Fusce nibh diam, ullamcorper et auctor sit amet, ullamcorper nec nunc. ','photo',0,1),
(7,'CARD 4','card_4.jpg','Fusce nibh diam, ullamcorper et auctor sit amet, ullamcorper nec nunc. ','photo',0,1),
(8,'CARD 5','card_5.jpg','Fusce nibh diam, ullamcorper et auctor sit amet, ullamcorper nec nunc. ','photo',0,1),
(9,'ICON 1','icon_1.jpg','Nam id massa ornare nulla vulputate interdum vel a tellus. Mauris sed porttitor lorem. ','app',0,1),
(10,'ICON 2','icon_2.jpg','Nam id massa ornare nulla vulputate interdum vel a tellus. Mauris sed porttitor lorem. ','app',0,1),
(11,'ICON 3','icon_3.jpg','Nam id massa ornare nulla vulputate interdum vel a tellus. Mauris sed porttitor lorem. ','app',0,1),
(12,'ICON 4','icon_4.jpg','Nam id massa ornare nulla vulputate interdum vel a tellus. Mauris sed porttitor lorem. ','app',0,1),
(13,'ICON 5','icon_5.jpg','Nam id massa ornare nulla vulputate interdum vel a tellus. Mauris sed porttitor lorem. ','app',0,1),
(14,'LOGO 1','logo_1.jpg','Ut sodales dolor et mauris dignissim euismod. ','ext',0,1),
(15,'LOGO 2','logo_2.jpg','Ut sodales dolor et mauris dignissim euismod. ','ext',0,1),
(16,'LOGO 3','logo_3.jpg','Ut sodales dolor et mauris dignissim euismod. ','ext',0,1),
(17,'LOGO 4','logo_4.jpg','Ut sodales dolor et mauris dignissim euismod. ','ext',0,1),
(18,'LOGO 5','logo_5.jpg','Ut sodales dolor et mauris dignissim euismod. ','ext',0,1),
(19,'LOGO 6','logo_6.jpg','Ut sodales dolor et mauris dignissim euismod. ','ext',0,1),
(20,'LOGO 7','logo_7.jpg','Ut sodales dolor et mauris dignissim euismod. ','ext',0,1),
(21,'WEB 1','web_1.jpg','Aenean nibh sapien, sodales vel ullamcorper sit amet, volutpat in orci.','web',0,1),
(22,'WEB 2','web_2.jpg','Aenean nibh sapien, sodales vel ullamcorper sit amet, volutpat in orci.','web',0,1),
(23,'WEB 3','web_3.jpg','Aenean nibh sapien, sodales vel ullamcorper sit amet, volutpat in orci.','web',0,1),
(24,'WEB 4','web_4.jpg','Aenean nibh sapien, sodales vel ullamcorper sit amet, volutpat in orci.','web',0,1);

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telpon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '0:mahasiswa;1:dosen',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mahasiswa_nim_unique` (`nim`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `mahasiswa` */

insert  into `mahasiswa`(`id`,`nama`,`telpon`,`nim`,`email`,`password`,`device`,`jenis_kelamin`,`alamat`,`token`,`foto`,`status`,`tipe`,`created_at`,`updated_at`) values 
(41,'Rita aprilia','081931453988','4317060018','rita.sundu@gmail.com','$2y$10$1ZfidrN9Xgfc2itysz8DtOOkmbHFd0q7iuvkb75EWO8F4TNy31ysW',NULL,'Perempuan',NULL,'OgnD6YyEmrZLTWQwUGTlFEUUW',NULL,'0','0','2019-07-22 15:45:43','2019-07-22 15:45:53'),
(2,'Ferry Ricardo Siagian','08111442065','4317060005','ferry.ricardo@gmail.com','$2y$10$6uWQfQw.uKpJYspJlAvedeWOElWsXdBlM.pzo5Lt4k3psuSukBgaW',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-15 02:11:05','2019-02-15 02:11:05'),
(3,'rizky awlia fajrin','12345','12345','12345@gmail.com','$2y$10$MmtoF5r19CcZSnEIZL8m.e1AANQjnb3VNwgBXRpkhiJHdd4bMj0wS',NULL,'Laki-laki',NULL,'6PEeR7ixNdgVPFtZijAIsNYIx',NULL,'0','0','2019-02-16 15:39:18','2019-07-24 14:03:13'),
(4,'Siti Nur Aiyah Jamil','082115480220','4317060020','sitinur.asiyahjamil4@gmail.com','$2y$10$Mu6M5kbmjW4EhgsFAVg5BOBFRqtttA7PBmRr3iJEVBsZO.rEPqejC',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-18 23:33:20','2019-02-18 23:33:20'),
(5,'AlHaris','085264376772','4317060002','alharisal8@gmail.com','$2y$10$oEYhA.GnBpw5fhHTSmSgUug/aKdHkAIfRuBxas53PQnQbWx3M2ws.',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-18 23:37:11','2019-02-18 23:37:11'),
(7,'Nurul Fatiah','08119221307','4317060013','fafathia@gmail.com','$2y$10$ki3bmmybcw5GrogHj8THYe9Vp8W7wozRCzZzGZ0Y71/DczpSotDbS',NULL,'Perempuan',NULL,'gLoHGnbCsAuOgGrtRVZAwAyTF',NULL,'0','0','2019-02-19 03:02:45','2019-02-23 07:04:02'),
(8,'Raesya Nugraha','081218565963','4317060014','raesyanugraha@gmail.com','$2y$10$Rq26XOSOWY83iAofhCalS.EaaRdTftqWuoFYlQWWaiR6pDZR1QXPi',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 03:03:41','2019-02-19 03:03:41'),
(9,'Muhammad Faishal Akbar','089601461462','4317060009','faishallakbar@gmail.com','$2y$10$gu6BKbKzmYoTcm9c3uY4L.YUSv2dmJZdd9ZutK2Yoj/.6jLMtEtbS',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 03:04:35','2019-02-19 03:04:35'),
(10,'Nurudien Irhami','085695966119','4317060012','nurudien.irhammi@gmail.com','$2y$10$/6MixNFcduk8j3.QG.ksXua1R2gyDfY8LSn62BkI9a7ocrQufF9IK',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 03:05:26','2019-02-19 03:05:26'),
(11,'Yusuf  Wardhani','08111082212','4317060021','yusufwardhani1304@gmail.com','$2y$10$/ZTWwIxKZWx5XthNsNmqHe9VPoyyt/Y0X7QCyaw3wDT/RWIfZGWY.',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 03:06:24','2019-02-19 03:06:24'),
(12,'Rodiyah Indah Lestari','081511270455','4317060019','rodiyahindahlestari@gmail.com','$2y$10$.8CAJQFjbLGDSwfX1RV9H.6xBcuP4RxhKhbbHEeEcC5L/g2oe13/K',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 03:07:12','2019-02-19 03:07:12'),
(13,'Disanrio Arif','081514195175','4317060004','disanrioarif@gmail.com','$2y$10$IGWKKy0KAmt7T9x.fDxLJ.5CqWYnuqAwLhe9mdnmsNVmQSXG4LYV6',NULL,'Laki-laki',NULL,'Nf7LmpseBSNLiulJyuQFySDuW',NULL,'0','0','2019-02-19 03:08:33','2019-02-23 07:00:02'),
(14,'Yoga Arie','085777908183','4317060008','yogaarir@gmail.com','$2y$10$l/hMY.icDuztqszM8w.0fua8JOQE50LdubUqoHS6pxagHOyXjJbSS',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 03:09:28','2019-02-27 02:20:49'),
(15,'Akhmarditya','081291011006','4317060001','akhmarditya029@gmail.com','$2y$10$sFyUxscOoBQPQtJfuIk30.I2k8w/48CM0S1OW9kA6xMgnjeJ1qQja',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 03:10:19','2019-02-19 03:10:19'),
(16,'Norma Maulidha Hasanah','081513759855','4317060011','normamaulidha.hasanah16@gmail.com','$2y$10$mcU5MCjmDw2WZEsPVhq10usGkXGb1LQhjwSC/UcAv75T6udiuD72y',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 03:11:14','2019-02-19 03:11:14'),
(17,'Ridwan','085775889427','4317060016','wantedaj48@gmail.com','$2y$10$WLafX/wj5f67WJfQupsvqu5IjSIRdbx.3fPaZZZP.6SVUxquSC2fO',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 04:43:58','2019-02-19 04:43:58'),
(18,'Yosep Sidik','089643117073','1803422035','yosepsidik23@gmail.com','$2y$10$43bDJL3/.3oOQgtvIGlqs.80MQLdtPGAiqVKqakry61eFyx15RmuW',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 12:18:34','2019-02-19 12:18:34'),
(19,'Sarah Fadillah','08119885868','1803422013','sarahfadillah31@gmail.com','$2y$10$9aQZbl9oFrvSR5CMpxyNq.1wlaN0lUls55xefgLkrF58qStd4ortK',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 12:24:52','2019-02-19 12:24:52'),
(20,'Zhafira Hapsari','085921890408','1803422018','zhafirahapsari@gmail.com','$2y$10$BlzCXKaF49.bUpXDFq7tNe6zlCE3BCSpzpEjA1fJgW7ztnm2Foci.',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 12:27:25','2019-02-19 12:27:25'),
(21,'Nabiela Hurul Ein','081932687264','1803422040','nabielahurulein@gmail.com','$2y$10$EsfDl.RsOESBrSyvqqn9CulKIp665FeRM2eqgAgpG6JQfBAk8BtwW',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 12:29:26','2019-02-19 12:29:26'),
(22,'Irfanul Hakim','0817888414','1803422003','irfanulhkm@gmail.com','$2y$10$ksZDgd.GRX6ksvXP4t8aYed8GYyanw77ET8qJTLsVTsXG2jHQTDAC',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 12:31:25','2019-02-19 12:31:25'),
(23,'Lello Nella Rosa','0895604992945','1803422024','lellonellarosa@gmail.com','$2y$10$pZCd5AAWxF881/hj8fKEbODB6f4ucw0t5a8UNQWoZ.H3KAupmInRq',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 12:33:34','2019-02-19 12:33:34'),
(24,'Nanda Wahyu Septiadi','082169009296','1803422011','nandawahyo@gmail.com','$2y$10$WGUo2HDMZP9RRCP6M/k9TOuZtGl.zkEZNV8ZEQIhTCaP/hsOW9Iuy',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 12:34:53','2019-02-19 12:34:53'),
(25,'Weni Fitri','081261896495','1803422036','wenifitri26@gmail.com','$2y$10$QW0DcVj/VEm.d.IuI0LT2.FKxlLD6RoVAtFKWBBRcw7GmMaYTsX3q',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 12:36:23','2019-02-19 12:36:23'),
(26,'Juliani Windy','085920740607','1803422033','julianiwindi379@gmail.com','$2y$10$5zdQ5po4/5nXxLFe1D/iaOv1qpmYCxvJs1p319GXLhxuMZ7LLjOUK',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 12:38:17','2019-02-19 12:38:17'),
(27,'firyal Lutfiyah ramandani','081316893837','1803422014','lutfiafiryal7@gmail.com','$2y$10$N.XAxspY8hggCyLfiakOguhIER5.yR2X37XVPdxdRgMM5pJbnSJR.',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 12:40:17','2019-02-19 12:40:17'),
(28,'Sulthana Safinatunnajah','081908144055','1803422006','ssafinatunnajah@gmail.com','$2y$10$9uT2oUtbQGZ9GR4eb8e5Bu43TdgvS99eWL9KXLe72flawX37/ce12',NULL,'Perempuan',NULL,'79xbW9FDNBLzXAMIxURIAxyNS',NULL,'0','0','2019-02-19 12:41:46','2019-02-22 02:36:52'),
(29,'Prillia Risma Choerunnisa','085710447981','1803422023','Prilliarisma07@gmail.com','$2y$10$2lORO7aCQYUuJJJaJ/i8TuyEpn7Na/DvPy0UhopgBNQh1eTXzqqwG',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-19 12:43:02','2019-02-19 12:43:02'),
(30,'Ginanjar Dwi S','085691311914','1803422008','ginanjar.dwi.santoso@gmail.com','$2y$10$79vdJ5i9DGiR2civvDL0E.BCxkxsFNahGA09vboDTyWMyxdE5Iefm',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 12:45:47','2019-02-19 12:45:47'),
(31,'Muhammad Syahid Hasan S','087884524291','1803422027','muhammadsyahid21@gmail.com','$2y$10$pF3VqBY9AhUnF/zPEKJGs.dtGJM2f1f3nAYf8uos46ajsDrg4W5ly',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 12:49:29','2019-02-19 12:49:29'),
(32,'Kalam Nababan','085210637885','1803422038','andynababan@gmail.com','$2y$10$wU.9OeuRiyxf.aTXW4763Og6UJMq1yhVuO66kBpxiVSzzTibP2/.u',NULL,'Laki-laki',NULL,NULL,NULL,'0','0','2019-02-19 12:50:23','2019-02-22 02:32:58'),
(34,'Harry Pribadi','08174897007','123456789','hari.pribadi@gmail.com','$2y$10$a2bKFYp8yiqujwo1Tqg.jurwg.ljB0UFETZiMGb5FOrg2MtHiqZLy',NULL,'Laki-laki',NULL,'UrJUx7CwvSwgHpDBpxsSnxxNW',NULL,'0','0','2019-02-22 02:32:38','2019-02-22 02:33:06'),
(35,'Yulismar','087876170435','300494','yulismar01@gmail.com','$2y$10$GoRcbM6I2K6QCDKUJPbOWeczUHPqOEIIFofj8gp3ChUCMznO8BOnu',NULL,'Perempuan',NULL,NULL,NULL,'0','0','2019-02-23 18:37:42','2019-07-22 14:49:38'),
(36,'M. Fathurahman','087886581012','197108242003121001','m.fathurahman@gmail.com','$2y$10$oaPigVZRLzKbOWi715AiAe/kFL.YsgGoAfteErvSzwrbWsvj0ACmG',NULL,'Laki-laki',NULL,NULL,NULL,'0','1','2019-02-24 04:31:09','2019-02-27 04:03:50'),
(37,'Hadi','085279959498','12345678','admin@gmail.com','$2y$10$dBzwoV4n7kGmTCf4R1tZxOBHPwSPJC4fxiVB0GFyl62oFgKzrHmCW',NULL,'Laki-laki',NULL,'NVxXEFEmxSnGErsDFKLDPMwVX',NULL,'0','0','2019-02-24 13:33:03','2019-02-25 09:43:08'),
(38,'Farkhan Rosi','085555555','1234567890','admin@gmail.com','$2y$10$2QYhiBiNCoqN//rDhc5aou1jtvJ2NwgZH.S0n0d7yxxNteESVeTC.',NULL,'Laki-laki',NULL,'SEYBdYgpqGcrVfKwPZYVCqFRO',NULL,'0','0','2019-02-25 10:37:11','2019-02-25 10:37:52'),
(39,'Ifaz Fachrul','0878040677','13101018','ifazfachrul@gmail.com','$2y$10$/3q7ewxHCtYx87X7jhFRGuRlKCFvFhLoc1v/0CNOn7DnOWvpMjMKK',NULL,'Laki-laki',NULL,'JpBwYoGFLOQnVxiDUXCoCoRvE',NULL,'0','0','2019-02-25 15:02:01','2019-02-25 15:53:20'),
(40,'Dosen baru','081929292929','1929192918292829','admin@adminl.com','$2y$10$Ge00vL8rhco77pP9iY9L8uXMi9ZPSqbIubtgsY/UR2icwYNcvvIna',NULL,'Laki-laki',NULL,'iuL9mkjqIBlIMnEHIqquMVYAy',NULL,'0','1','2019-02-27 03:59:47','2019-02-27 04:07:40');

/*Table structure for table `mata_kuliah` */

DROP TABLE IF EXISTS `mata_kuliah`;

CREATE TABLE `mata_kuliah` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dosen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ruang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hari` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waktu_mulai` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waktu_selesai` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `mata_kuliah` */

insert  into `mata_kuliah`(`id`,`nama`,`dosen`,`ruang`,`hari`,`tahun`,`sks`,`waktu_mulai`,`waktu_selesai`,`created_at`,`updated_at`) values 
(1,'Statistik','Evan','A-16',NULL,NULL,'3','15:00','18:00','2019-02-18 09:51:31','2019-02-18 09:51:31'),
(2,'Teknik Kompresi Data','Agus W','D.302',NULL,NULL,NULL,'18:15','19:45','2019-02-18 23:12:03','2019-02-18 23:12:03'),
(3,'Sistem Selular','Isti Yuliarso','D.302',NULL,NULL,NULL,'19:45','21:15','2019-02-18 23:13:35','2019-02-18 23:13:35'),
(6,'Matematika Teknik','Sutanto','D.302','Thu',NULL,NULL,'17:00','21:15','2019-02-18 23:19:58','2019-02-20 10:13:06'),
(7,'Jaringan Komunikasi Pita Lebar','Hendra','D.302','Mon','2019',NULL,'06:00','11:00','2019-02-18 23:20:54','2019-02-24 13:34:00'),
(10,'Komputasi dan Komunikasi Multimedia','Evan','A-16','Thu','----',NULL,'05:00','09:00','2019-02-19 15:16:08','2019-03-13 23:51:36'),
(32,'Sistem Embedded','Fathurahman','D.302','Wed','2019',NULL,'09:00','11:00','2019-02-25 15:52:31','2019-02-27 02:20:17'),
(33,'alokasi ip plan','rita aprilia','D300','Tue','2019',NULL,'06:00','10:00','2019-07-22 23:52:41','2019-07-22 23:52:41'),
(34,'Sistem TV','Dandun Widhiantoro','D311','Sat','2019',NULL,'10:00','15:39','2019-07-24 08:05:11','2019-07-27 03:31:35');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(8,'2019_02_18_192437_insert_table_presensi',3),
(7,'2019_02_18_174321_create_table_presensi',3),
(6,'2019_02_17_231225_create_table_config',2),
(5,'2019_02_17_221006_create_table_relasi_matkul_mahasiswa',2),
(4,'2019_02_17_220451_create_table_mata_kuliah',2),
(3,'2019_02_14_180311_create_table_mahasiswa',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(1,'2014_10_12_000000_create_users_table',1),
(9,'2019_02_19_091653_insert_table_matkul_day',4),
(10,'2019_02_20_214007_insert_tahun_in_matkul',5),
(11,'2019_02_23_232619_insert_to_table_mahasiswa',6);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `presensi` */

DROP TABLE IF EXISTS `presensi`;

CREATE TABLE `presensi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mahasiswa_id` int(10) unsigned NOT NULL,
  `mata_kuliah_id` int(10) unsigned NOT NULL,
  `via` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0:raspbery;1:mobile',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0:not;1:sudah absen',
  `tanggal_presensi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jam_presensi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waktu_mulai` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waktu_selesai` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `presensi_mahasiswa_id_foreign` (`mahasiswa_id`),
  KEY `presensi_mata_kuliah_id_foreign` (`mata_kuliah_id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `presensi` */

insert  into `presensi`(`id`,`mahasiswa_id`,`mata_kuliah_id`,`via`,`status`,`tanggal_presensi`,`jam_presensi`,`waktu_mulai`,`waktu_selesai`,`created_at`,`updated_at`) values 
(1,3,10,'0','0','2019-02-19','23:35','22:00','23:59','2019-02-19 16:35:33','2019-02-19 16:35:33'),
(2,3,10,'0','0','2019-02-19','23:36','22:00','23:59','2019-02-19 16:36:44','2019-02-19 16:36:44'),
(3,3,10,'1','1','2019-02-19','23:39','22:00','23:59','2019-02-19 16:39:06','2019-02-19 16:39:06'),
(4,3,10,'1','1','2019-02-20','06:19','05:00','08:00','2019-02-19 23:19:55','2019-02-19 23:19:55'),
(5,3,10,'1','1','2019-02-20','06:27','05:00','08:00','2019-02-19 23:27:07','2019-02-19 23:27:07'),
(6,3,10,'0','0','2019-02-20','06:42','05:00','08:00','2019-02-19 23:42:31','2019-02-19 23:42:31'),
(7,3,10,'0','0','2019-02-20','06:52','05:00','08:00','2019-02-19 23:52:00','2019-02-19 23:52:00'),
(8,3,10,'0','0','2019-02-20','07:11','05:00','08:00','2019-02-20 00:11:33','2019-02-20 00:11:33'),
(9,3,10,'1','1','2019-02-20','08:06','05:00','09:00','2019-02-20 01:06:11','2019-02-20 01:06:11'),
(10,3,7,'1','1','2019-02-20','08:24','08:00','10:00','2019-02-20 01:24:53','2019-02-20 01:24:53'),
(14,32,6,'0','0','2019-02-20','17:10','17:00','21:15','2019-02-20 10:10:56','2019-02-20 10:10:56'),
(15,3,6,'1','1','2019-02-20','17:11','17:00','21:15','2019-02-20 10:11:33','2019-02-20 10:11:33'),
(36,1,7,'1','1','2019-02-23','21:54','20:00','23:00','2019-02-23 14:54:54','2019-02-23 14:54:54'),
(37,1,7,'1','1','2019-02-23','21:54','20:00','23:00','2019-02-23 14:54:55','2019-02-23 14:54:55'),
(38,3,7,'1','1','2019-02-23','21:55','20:00','23:00','2019-02-23 14:55:33','2019-02-23 14:55:33'),
(68,39,32,'1','1','2019-02-25','22:53','22:00','23:00','2019-02-25 15:53:25','2019-02-25 15:53:25'),
(69,1,32,'1','1','2019-02-26','09:30','09:00','11:00','2019-02-26 02:30:42','2019-02-26 02:30:42'),
(75,1,32,'1','1','2019-02-27','10:31','09:00','11:00','2019-02-27 03:31:56','2019-02-27 03:31:56'),
(76,3,10,'1','1','2019-03-14','06:53','05:00','09:00','2019-03-13 23:53:21','2019-03-13 23:53:21'),
(77,41,33,'1','1','2019-07-23','06:53','06:00','10:00','2019-07-22 23:53:46','2019-07-22 23:53:46'),
(78,41,34,'1','1','2019-07-26','23:05','23:00','23:39','2019-07-26 16:05:12','2019-07-26 16:05:12'),
(79,41,34,'1','1','2019-07-27','10:32','10:00','15:39','2019-07-27 03:32:00','2019-07-27 03:32:00');

/*Table structure for table `relasi_matkul_mahasiswa` */

DROP TABLE IF EXISTS `relasi_matkul_mahasiswa`;

CREATE TABLE `relasi_matkul_mahasiswa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mahasiswa_id` int(10) unsigned NOT NULL,
  `mata_kuliah_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `relasi_matkul_mahasiswa_mahasiswa_id_foreign` (`mahasiswa_id`),
  KEY `relasi_matkul_mahasiswa_mata_kuliah_id_foreign` (`mata_kuliah_id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `relasi_matkul_mahasiswa` */

insert  into `relasi_matkul_mahasiswa`(`id`,`mahasiswa_id`,`mata_kuliah_id`,`created_at`,`updated_at`) values 
(2,2,1,'2019-02-18 09:54:22','2019-02-18 09:54:22'),
(3,3,1,'2019-02-18 09:54:32','2019-02-18 09:54:32'),
(4,3,10,'2019-02-19 16:36:26','2019-02-19 16:36:26'),
(7,3,6,'2019-02-21 00:13:26','2019-02-21 00:13:26'),
(20,1,7,'2019-02-23 14:51:36','2019-02-23 14:51:36'),
(21,2,7,'2019-02-23 14:51:36','2019-02-23 14:51:36'),
(22,3,7,'2019-02-23 14:51:36','2019-02-23 14:51:36'),
(30,37,7,'2019-02-24 13:34:26','2019-02-24 13:34:26'),
(53,39,32,'2019-02-25 15:52:41','2019-02-25 15:52:41'),
(54,1,32,'2019-02-26 02:30:29','2019-02-26 02:30:29'),
(58,14,32,'2019-02-27 02:20:36','2019-02-27 02:20:36'),
(59,41,33,'2019-07-22 23:53:36','2019-07-22 23:53:36'),
(64,41,34,'2019-07-26 16:04:14','2019-07-26 16:04:14'),
(63,36,34,'2019-07-26 16:04:04','2019-07-26 16:04:04');

/*Table structure for table `tb_blogcategories` */

DROP TABLE IF EXISTS `tb_blogcategories`;

CREATE TABLE `tb_blogcategories` (
  `CatID` int(6) NOT NULL AUTO_INCREMENT,
  `name` text,
  `slug` text,
  `descr` text,
  `enable` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`CatID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `tb_blogcategories` */

insert  into `tb_blogcategories`(`CatID`,`name`,`slug`,`descr`,`enable`) values 
(1,'Tutorial thea','tutorial-thea',NULL,'1'),
(2,'News','news','','1'),
(3,'API','api',NULL,'1'),
(4,'Lifestyle','Lifestyle',NULL,'1'),
(5,'Food','food',NULL,'1'),
(6,'Travel World',NULL,NULL,'1');

/*Table structure for table `tb_blogcomments` */

DROP TABLE IF EXISTS `tb_blogcomments`;

CREATE TABLE `tb_blogcomments` (
  `commentID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `blogID` int(11) DEFAULT NULL,
  `name` text,
  `email` text,
  `comment` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`commentID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `tb_blogcomments` */

insert  into `tb_blogcomments`(`commentID`,`parentID`,`user_id`,`blogID`,`name`,`email`,`comment`,`created`) values 
(10,NULL,1,2,NULL,NULL,'tester','2014-07-12 05:41:55'),
(13,NULL,1,1,NULL,NULL,'I want more feature','2014-07-23 09:04:44'),
(14,NULL,1,2,NULL,NULL,'abc','0000-00-00 00:00:00'),
(15,NULL,1,2,NULL,NULL,'def ghi jik','0000-00-00 00:00:00'),
(16,NULL,1,1,NULL,NULL,'mee too','2015-05-19 01:21:47'),
(18,NULL,1,3,NULL,NULL,'test','2015-07-16 15:15:15');

/*Table structure for table `tb_blogs` */

DROP TABLE IF EXISTS `tb_blogs`;

CREATE TABLE `tb_blogs` (
  `blogID` int(11) NOT NULL AUTO_INCREMENT,
  `CatID` int(6) DEFAULT NULL,
  `title` text,
  `slug` text,
  `content` text,
  `created` datetime DEFAULT NULL,
  `tags` text,
  `status` enum('publish','unpublish','draft') DEFAULT 'draft',
  `image` varchar(100) DEFAULT NULL,
  `entryby` int(11) DEFAULT NULL,
  PRIMARY KEY (`blogID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tb_blogs` */

insert  into `tb_blogs`(`blogID`,`CatID`,`title`,`slug`,`content`,`created`,`tags`,`status`,`image`,`entryby`) values 
(1,2,'Working With Artisan & Composer','working-with-artisan-composer','<p>{{{ customers::index }}}</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.&nbsp;Lorem ipsum dolor sit amet, consectetur</p>\r\n<hr />\r\n<p>adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>','2014-02-22 00:00:00','7-things, tutorial , generator , builder','unpublish',NULL,1),
(2,2,'Bootstrap 3: What you need to know','bootstrap-3-what-you-need-to-know','<p><span style=\"color: #717171;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.</span></p>\r\n<p><span style=\"color: #717171;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.</span></p>\r\n<p><span style=\"color: #717171;\">&nbsp;</span></p>','2014-02-22 00:00:00','lorem, ipsum , solades','publish',NULL,1),
(3,1,'Creating New Pages','creating-new-pages','<p><span style=\"color:rgb(113,113,113);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.Â  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.Â </span><span style=\"color:rgb(113,113,113);\"><br /></span></p>\r\n\r\n<p><span style=\"color:rgb(113,113,113);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.Â </span><span style=\"color:rgb(113,113,113);\"><br /></span></p>','2014-02-23 00:00:00','pages, page builder , builder','publish',NULL,1),
(4,1,'Creating Modules','creating-modules','<p><span style=\"color:rgb(113,113,113);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.Â  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.Â </span><span style=\"color:rgb(113,113,113);\"><br /></span></p>','2014-02-23 00:00:00','module , builder','publish',NULL,1),
(5,1,'New from our blog','new-from-our-blog','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.</p>','2014-08-11 12:30:01','no tags','publish',NULL,1);

/*Table structure for table `tb_finger_data` */

DROP TABLE IF EXISTS `tb_finger_data`;

CREATE TABLE `tb_finger_data` (
  `user_id` int(11) NOT NULL,
  `finger_id` int(11) DEFAULT NULL,
  `finger_data` text,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_finger_data` */

insert  into `tb_finger_data`(`user_id`,`finger_id`,`finger_data`) values 
(2,1,'565F33A3D4CF5E09F768A7C8C3055E00F85701C82AE3735CC0413709AB71F0F6145592E800D1B64686D56152A1DCAB64BAD0AD133E194A5D9592FEE7CCC9576FC96C031B391711733D4023583F2498B65AC99B124BAA15556846EB1D8CA94E317781F50B5A24E05C8E3BA2E97FF0E78264BFDA41DC04D42A57411B88D15D174D7DE8387981DCB5C5CAB3F0E49CE1907875FDE840CF04D3A93FF9F0D1714EEBFBD7B0E9FA5261295A814089098CC1E3E4ED9C242FE92E7499324D95A16FE3FAD8D4B744C72D0AB612D9FF0290B9FBD441FD80A2B3C0416FF9CF57C8E869FD768398E3EEA3994548A250B7BAE4A54EBCBADA7C1A6E9B921F17497BFF05F2A624DD51E937B09A396455EEAD15E440A819B66EA1031E392C7162AB86724AA8BB265680B479559AF27513D03F7ACFE3947F7D411AB5417FF347884A7E4D46D1DB5B97160D9747CEB9D0F6FF8C7FBEA344BBE99D4EB6771CEFBBDD088D5B9F3171EB0238A168E1B97926D49B1B3F36237C9914471AD96F6F00F88001C82AE3735CC0413709AB71709514559274AF085647C652CF0FEE1BBDF44AFDDA43F29FF87C133D3043131A468958E0F0A8D86A673F5D11B41E64FF84C6C71BDDB5A7AE45164C3E5B3CD24066F2DAC26401DC3F7D7C5FF219529592A28F0593C28E1C0CB5D0547A9BEF06BED5226D263C6F7D4CD8086463B56995F24A07E0FFF69049EEF2B0C86ADC8BC4A2EEE2CE29FC38F0CCC2590DB71F62BAA3C106089FD0759633288CE1D9AB55E61820F03F52D685BEB657A10F22DC73B6F2C6430BC743B2E10136D5D842869D4B6C913A300E841D9C58E26FF459DAB3371D894027C337CBDCA6740DB07ED32CA0767311563BA998EAB9C29EA33E2DC6CE4D6D196C4D768DDA5F1B75A9CC9A7E27651E99057194E73E64BBAD41B7523C8A21A1AC0E90F435AFEB2077FDE08AE538FD3D0BC03C3D7B31BD35CD0276C740FA38675E2F15524AD02A171577F853A811D9A21E4CF4CEC6553DD9A7E0EB7F8C83EBF1DCDBF84BB4C56B61CB4EAB592F9C85CB7AD2519A6BAD5CBAB1179B35CE7CAB08F08EC84B6F00F88001C82AE3735CC0413709AB71F08E14559264E8417B897E0B98370449E21BF93FC4D1A98DC914B2C076AA8FF170B10E6371C931D7E8C085000FBAECE951D1783E4FA9ADB8216F9595F67BD11B7ADC27C9E43A9ED53CB07211A8EC8B3D7A058AC35115673A622C9E25DED60B7F367AF7D782DAAB89B7F93CE253A241E529328DFEE0AB46B1582407CD52AF4CA052E41CC7B372BCE97B3C97ED61A256F31326ABA52F0FBFFA121C1FE660A5DFEDD347703AD0B9080D5EC5A9B5CF8B514955DEB7097CB038BCA0A053B5619130CA410170195238B8171BB06AAEAFE0D88CFE2E58BC44E8B8AB376FB2910F9B03D263C95A7DC61475A8889DA6A00CE803B6BDECEC97A8E8BD35BC2FF04CCE6EF3FE359B0746F61E780EE0BCF1DE1FE5968A4C925C35A0A01C5621DA94F29BEB1A5452A534306EFB453C4AEE509209726B9DE213C58019DD03AE4804B9D68D1BBB3EA0732D4854DD44D0707F39321C51B22E2D929F0219B4C444B0999E7C5C096F612E31FA98090A0C38E4CD9B7ED0C84CDF88BBFE8BE86F00E87E01C82AE3735CC0413709AB7170AE1455920279838B767FF70F680255CCA4AD8C79026077E9A27F795164D372D228A33BE49BF5ED3130DEAA5DB81EDB626ABF798CDE08486F0BAF70F605D94288830DCAAEBD7E2F28070156F35A051129209B3F8A936E6A8F493953685E4C4B9EAF7F317174C37C5BC106F919EEC3AD007BAF9750D15779A80047B139C0FB893B2FC53CF375F5352C9A563D3239DE6A06E87D16CC4D0F9AB565A080F06673D14D6DBAB2FCF3520603C7AEBC1FC1BE4C117A448DF671CBF0B285CB902BA6FF1702BA2499B87C4B8BC7959DCD3B050ED04971707334B16FF297E016DB63DFACBA1F466F28E2A6103A5E77A2C10544E20A72D86081F70B4082FA6B43757B7085EE48D50FFA69749FD381E9CE31089B59ACE8531AB73D2341E1CDAE9BC4BE85CF4CE64A73B379DFF66758FC83EEAF0C9CB59F4DDEC025456EC8136C679D58C85497D49948B8E3CE68A7C1475A1F0DA34BA3A71A170E787DEDA0FCAAFBC9E9FA0795BC6D92A89B167D1AA755A732EDCCBEB6D6E1336F0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'),
(3,1,'562332A9D9BF2B0FF112A6BDC4042300F87801C82AE3735CC0413709AB71B0801455922C5F2E68980A786E2D744D4392DA908F5836946EFDB2BD29BF2F47AC58763743FEAE2E99F5FA8F8FCFBB678A2BDEC4B20DD9F2E044D4D67E4E0BB1227A122B343646D2E3ED46161DAB34C4A677AEEC0B4C34E146FEB6BC660568DA23FB6A6D5F8B3AE45671DCED9FF3BA3F0D6D905993B8F3DD096F9B5DE04B585AD8B4E6E2DF6149727ED930CB3E30C40817FE636ED8E273498A5518A4EEB2155826CC8554C9A663E1C2D8EAE98C7652D29674B844754AADBBD15281F9D82419A614F36F14A43C34EFA2830B4EF9A5A144C76269D06D3F56F0FC16D04C88450B68EF5AA95AF89D3AE2C4EFC937C908432FA5DB665903C999C7C3A10478DB585C2CD58DF0FB638D10F35545CA7EE53470A68A9019F8A58AB2196681337AA87EFA8198E7604CC4E2015CFBAF8AB2EAAD5551CCEC24D42F1678B474D714B80120AF09A22DED7C3565FAF2B18AC92FF89D74B1B568ADC64AFA0522D72EB77635981A5CBA28EF67E79ADCB184C7D426F4105135C8EA56B413C42C6F00F86A01C82AE3735CC0413709AB71B09814559223CF468EEFBB321B281AD3B133BE573ADD81117A5B2837B549FCA51772C4022576D2D282154ECCDBE4446D61558D82A2945B6F1ACCA951CA0949D6BF11704A54BBBA4581E626BA53804794FABF35EEECE909397BA2F3D23E0613D8BFB4EEC8EBAB41532F11F584F42BE1205AD93F1BA89D0B0C03B1C8DA6A277E15FCACFC148A2476E9C8162DB5DA02E138F37FF41BAC10F2D873A0978709AF4C9482C700F1F2860D93F786FBE613B0921453FC5739CC073C0103CCD54DFE57063A774685E1B82AD068CE49105295EE4344C89015843063DA419D0006644FA5DC5BFCBB434E68B232CA4C113B8654AF5FAE50BF5F3F937C208F4B199E4663B8FB50FF6F65F60F65F1580354A1B21D4F10DDCF87B9A456A46227C89B7C2DE4826A7B09D5E13E9E24F2814F110A17418B9DCAC2650E7741B4DAD19AC51FDF94AC629847C82D1D473EBF146296C38531A2A2D21B2FCA93560AEACD5B141BB67C77E86F00F81A01C82AE3735CC0413709AB71B099145592C94FBDB0106A426D5699F78CADDAEE15177A2EC12A86FBCE30499D30803EC432F70DED10B57D84CF470BA0B19A90629AB4B4AEE9DD86818AD0C08D773CDE72A3A353FE22B88E49519A31252F4D7A4291188B7A5F0C00BBAB35DCFE45842BF818C2335DCD315F2227C18544D43DAA111CDDC8AD39175E4DF89C88E3CBA15C6DA7120B3BC24B6FA8B4DC493512CD25C4FAA021420D323E019BC2F3B2E0706D2FD203C82489BE53646ABB0BBA9DDD8851E6AEE6EB64E136651F20D8BFDA7BCF403C51346CE916C1F0D8A0B6F116966FB381164C7AEB3862D1530596025BE5B7318CB34FEBBB40B7EAF1250A94E5DD49A9B11605BB7B1FFBE3A175B23E1540DB0AD5A72E57DE94C456B9F82A6F00E85401C82AE3735CC0413709AB71B099145592D846F4BB24FCCA45AF98BF465982F8EEB8F621D388A7152F0898F65FC8A2610081B82419D4544E5DA53E0B0C323BD33E5BD6632E99EF199D7F6FD5385E4749BAAC53A16AD3C2A57664D363059C0CF95695D4FAD284192C136A06890CD3EE678D06C86F63FD37F4868FBF086E77BCFD4368418A144A61F5C5A84464605CE826DE46F30F9FAB027C42EDE2CFE1914C25D65A3F920EA54FC8E8FC96B2C7A3F8F90B6E9176B66F61E823D22EDE1E2906D668B3B63C70837ED64996412432D16E7522F6D4EDA9BB6C2E816664785BA2F2655C8C28FF3A23ABC7D8D8B37809C98EFCB8B0B4EDA2C7A11CA7802AB5F38366972F263BBE0A7C6F9E38F3DCA1F30FDAB36F5DA578ACD4D1DE82B82BE8C5B5C3EF5504E41695022D202EE510F79DDC2E58B069F8BA4AB2F9AC323703D04EE993C68205A9BEB710AF29A5694AB356F1C7AB10B7856D1B6F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');

/*Table structure for table `tb_groups` */

DROP TABLE IF EXISTS `tb_groups`;

CREATE TABLE `tb_groups` (
  `group_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `level` int(6) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tb_groups` */

insert  into `tb_groups`(`group_id`,`name`,`description`,`level`) values 
(1,'Superadmin','Root Superadmin , should be as top level group',1),
(2,'Administrator','Administrator level, level No 2',2),
(3,'Users','<p>Users as registered / member</p>',3);

/*Table structure for table `tb_groups_access` */

DROP TABLE IF EXISTS `tb_groups_access`;

CREATE TABLE `tb_groups_access` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `group_id` int(6) DEFAULT NULL,
  `module_id` int(6) DEFAULT NULL,
  `access_data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=439 DEFAULT CHARSET=utf8;

/*Data for the table `tb_groups_access` */

insert  into `tb_groups_access`(`id`,`group_id`,`module_id`,`access_data`) values 
(166,1,2,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(167,2,2,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(168,3,2,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(169,1,8,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(170,2,8,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(171,3,8,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(199,1,7,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(200,2,7,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(201,3,7,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(202,1,1,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(203,2,1,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(204,3,1,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(223,1,11,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(224,2,11,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(225,3,11,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(277,1,14,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(278,2,14,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(279,3,14,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(286,1,17,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(287,2,17,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(288,3,17,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(304,1,22,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(305,2,22,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(306,3,22,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(310,1,25,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(311,2,25,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(312,3,25,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(331,1,36,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(332,2,36,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(333,3,36,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(334,1,37,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(335,2,37,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(336,3,37,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(337,1,38,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(338,2,38,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(339,3,38,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(343,1,40,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(344,2,40,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(345,3,40,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(349,1,42,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(350,2,42,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(351,3,42,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(352,1,43,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(353,2,43,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(354,3,43,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(355,1,44,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(356,2,44,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(357,3,44,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(367,1,45,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(368,2,45,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(369,3,45,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(370,1,46,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(371,2,46,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(372,3,46,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(373,1,47,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(374,2,47,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(375,3,47,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(379,1,49,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(380,2,49,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(381,3,49,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(433,1,65,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(434,2,65,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(435,3,65,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(436,1,66,'{\"is_global\":\"1\",\"is_view\":\"1\",\"is_detail\":\"1\",\"is_add\":\"1\",\"is_edit\":\"1\",\"is_remove\":\"1\",\"is_excel\":\"1\"}'),
(437,2,66,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}'),
(438,3,66,'{\"is_global\":\"0\",\"is_view\":\"0\",\"is_detail\":\"0\",\"is_add\":\"0\",\"is_edit\":\"0\",\"is_remove\":\"0\",\"is_excel\":\"0\"}');

/*Table structure for table `tb_logs` */

DROP TABLE IF EXISTS `tb_logs`;

CREATE TABLE `tb_logs` (
  `auditID` int(20) NOT NULL AUTO_INCREMENT,
  `ipaddress` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  `task` varchar(50) DEFAULT NULL,
  `note` text,
  `logdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`auditID`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=latin1;

/*Data for the table `tb_logs` */

insert  into `tb_logs`(`auditID`,`ipaddress`,`user_id`,`module`,`task`,`note`,`logdate`) values 
(87,'127.0.0.1',1,'customer','save',' ID : 1  , Has Been Changed Successfull','2014-08-10 12:08:53'),
(88,'127.0.0.1',1,'customer','save',' ID : 1  , Has Been Changed Successfull','2014-08-10 12:09:06'),
(89,'127.0.0.1',1,'customer','save',' ID : 1  , Has Been Changed Successfull','2014-08-10 12:12:03'),
(90,'127.0.0.1',1,'customer','save',' ID : 1  , Has Been Changed Successfull','2014-08-10 12:12:16'),
(91,'127.0.0.1',1,'customer','save',' ID : 1  , Has Been Changed Successfull','2014-08-10 12:12:32'),
(92,'127.0.0.1',1,'customer','save',' ID : 1  , Has Been Changed Successfull','2014-08-10 12:13:13'),
(93,'127.0.0.1',1,'eventcalendar','save','New Entry row with ID : 5  , Has Been Save Successfull','2014-08-11 10:40:16'),
(94,'127.0.0.1',1,'eventcalendar','save',' ID : 5  , Has Been Changed Successfull','2014-08-11 10:41:11'),
(95,'127.0.0.1',1,'eventcalendar','save','New Entry row with ID : 6  , Has Been Save Successfull','2014-08-11 10:43:59'),
(96,'127.0.0.1',1,'blogadmin','save','New Entry row with ID : 5  , Has Been Save Successfull','2014-08-11 12:34:23'),
(97,'127.0.0.1',1,'blogadmin','save',' ID : 5  , Has Been Changed Successfull','2014-08-11 12:34:36'),
(98,'127.0.0.1',1,'blogadmin','save',' ID : 5  , Has Been Changed Successfull','2014-08-11 12:34:55'),
(99,'127.0.0.1',1,'blogadmin','save',' ID : 5  , Has Been Changed Successfull','2014-08-11 12:36:52'),
(100,'127.0.0.1',1,'blogadmin','save',' ID : 2  , Has Been Changed Successfull','2014-08-14 14:11:59'),
(101,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 14:13:05'),
(102,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 14:14:20'),
(103,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 14:15:24'),
(104,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 14:17:13'),
(105,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 14:17:30'),
(106,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 14:44:21'),
(107,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 14:44:53'),
(108,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 14:47:43'),
(109,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 18:02:29'),
(110,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 18:06:36'),
(111,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-14 18:29:17'),
(112,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-15 09:54:11'),
(113,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-15 09:54:38'),
(114,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-15 10:00:41'),
(115,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-15 10:40:38'),
(116,'127.0.0.1',1,'eventcalendar','save',' ID : 5  , Has Been Changed Successfull','2014-08-16 11:11:41'),
(117,'127.0.0.1',1,'eventcalendar','save',' ID : 5  , Has Been Changed Successfull','2014-08-16 11:11:52'),
(118,'127.0.0.1',1,'eventcalendar','destroy','ID : 5  , Has Been Removed Successfull','2014-08-16 11:12:06'),
(119,'127.0.0.1',1,'eventcalendar','save','New Entry row with ID : 7  , Has Been Save Successfull','2014-08-16 11:12:26'),
(120,'127.0.0.1',1,'blogadmin','save',' ID : 5  , Has Been Changed Successfull','2014-08-16 18:14:16'),
(121,'127.0.0.1',1,'customer','save',' ID : 1  , Has Been Changed Successfull','2014-08-16 18:16:34'),
(122,'127.0.0.1',1,'customer','save',' ID : 1  , Has Been Changed Successfull','2014-08-16 18:18:25'),
(123,'127.0.0.1',1,'customer','save','New Entry row with ID : 124  , Has Been Save Successfull','2014-08-16 18:20:12'),
(124,'127.0.0.1',1,'customer','save',' ID : 124  , Has Been Changed Successfull','2014-08-16 18:22:34'),
(125,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-19 10:20:34'),
(126,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-19 10:22:12'),
(127,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-19 12:15:11'),
(128,'127.0.0.1',1,'blogadmin','save',' ID : 1  , Has Been Changed Successfull','2014-08-19 12:18:08'),
(129,'127.0.0.1',1,'eventcalendar','save','New Entry row with ID : 8  , Has Been Save Successfull','2014-09-04 15:15:22'),
(130,'127.0.0.1',1,'blogadmin','save',' ID : 5  , Has Been Changed Successfull','2014-09-05 14:54:34'),
(131,'127.0.0.1',1,'cc','save',' ID : 1  , Has Been Changed Successfull','2014-09-06 13:40:02'),
(132,'127.0.0.1',1,'cc','save',' ID : 1  , Has Been Changed Successfull','2014-09-06 13:42:02'),
(133,'127.0.0.1',1,'cc','save',' ID : 2  , Has Been Changed Successfull','2014-09-06 13:42:58'),
(134,'127.0.0.1',1,'cc','save',' ID : 1  , Has Been Changed Successfull','2014-09-06 13:43:07'),
(135,'127.0.0.1',1,'cc','save',' ID : 41  , Has Been Changed Successfull','2014-09-06 13:53:14'),
(136,'127.0.0.1',1,'cc','save',' ID : 74  , Has Been Changed Successfull','2014-09-06 14:01:04'),
(137,'127.0.0.1',1,'cc','save',' ID : 50  , Has Been Changed Successfull','2014-09-06 14:01:23'),
(138,'127.0.0.1',1,'cc','save',' ID : 39  , Has Been Changed Successfull','2014-09-06 14:01:51'),
(139,'127.0.0.1',1,'cc','save',' ID : 68  , Has Been Changed Successfull','2014-09-06 14:37:17'),
(140,'127.0.0.1',1,'cc','save',' ID : 68  , Has Been Changed Successfull','2014-09-06 14:37:33'),
(141,'127.0.0.1',1,'cc','save',' ID : 68  , Has Been Changed Successfull','2014-09-06 14:37:42'),
(142,'127.0.0.1',1,'cc','destroy','ID : 1  , Has Been Removed Successfull','2014-09-06 14:53:43'),
(143,'127.0.0.1',1,'cc','destroy','ID : 2  , Has Been Removed Successfull','2014-09-06 15:05:46'),
(144,'127.0.0.1',1,'cc','destroy','ID : 2,3,4,5,6,7,8,9,10,11  , Has Been Removed Successfull','2014-09-06 15:06:16'),
(145,'127.0.0.1',1,'cc','destroy','ID : 73  , Has Been Removed Successfull','2014-09-06 15:10:34'),
(146,'127.0.0.1',1,'cc','destroy','ID : 4  , Has Been Removed Successfull','2014-09-06 15:11:20'),
(147,'127.0.0.1',1,'cc','destroy','ID : 12,49,54,44,19,25,42  , Has Been Removed Successfull','2014-09-06 15:14:30'),
(148,'127.0.0.1',1,'cc','destroy','ID : 67,77  , Has Been Removed Successfull','2014-09-06 15:15:55'),
(149,'127.0.0.1',1,'cc','save',' ID : 2  , Has Been Changed Successfull','2014-09-06 15:49:19'),
(150,'127.0.0.1',1,'cc','save',' ID : 2  , Has Been Changed Successfull','2014-09-06 15:51:10'),
(151,'127.0.0.1',1,'cc','save',' ID : 2  , Has Been Changed Successfull','2014-09-06 15:54:03'),
(152,'127.0.0.1',1,'cc','save',' ID : 2  , Has Been Changed Successfull','2014-09-06 15:54:50'),
(153,'127.0.0.1',1,'cc','destroy','ID : 68,78  , Has Been Removed Successfull','2014-09-06 16:35:00'),
(154,'127.0.0.1',1,'cc','destroy','ID : 9  , Has Been Removed Successfull','2014-09-06 16:38:27'),
(155,'127.0.0.1',1,'cc','save',' ID : 38  , Has Been Changed Successfull','2014-09-06 19:23:59'),
(156,'127.0.0.1',1,'invoiceline','save',' ID : 128  , Has Been Changed Successfull','2014-09-08 10:19:30'),
(157,'127.0.0.1',1,'invoiceline','save',' ID : 127  , Has Been Changed Successfull','2014-09-08 10:23:15'),
(158,'127.0.0.1',1,'invoiceline','save',' ID : 127  , Has Been Changed Successfull','2014-09-08 10:23:57'),
(159,'127.0.0.1',1,'invoiceline','save',' ID : 127  , Has Been Changed Successfull','2014-09-08 10:24:17'),
(160,'127.0.0.1',1,'invoiceline','destroy','ID : 135  , Has Been Removed Successfull','2014-09-08 10:27:49'),
(161,'127.0.0.1',1,'cc','save',' ID : 38  , Has Been Changed Successfull','2014-09-08 13:41:17'),
(162,'127.0.0.1',1,'invoiceline','save',' ID : 127  , Has Been Changed Successfull','2014-09-08 13:42:59'),
(163,'127.0.0.1',1,'employee','save',' ID : 1  , Has Been Changed Successfull','2014-09-08 17:28:59'),
(164,'127.0.0.1',1,'cc','destroy','ID : 124  , Has Been Removed Successfull','2014-09-09 00:54:31'),
(165,'127.0.0.1',1,'cc','destroy','ID : 125  , Has Been Removed Successfull','2014-09-09 09:11:23'),
(166,'127.0.0.1',1,'users','save','New Entry row with ID :   , Has Been Save Successfull','2015-01-17 10:20:12'),
(167,'127.0.0.1',1,'users','save','New Entry row with ID :   , Has Been Save Successfull','2015-01-17 10:20:37'),
(168,'127.0.0.1',1,'users','destroy','ID : 4  , Has Been Removed Successfull','2015-01-17 10:20:51'),
(169,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 14:51:25'),
(170,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 15:09:10'),
(171,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 15:22:02'),
(172,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 15:23:58'),
(173,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 16:08:08'),
(174,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 16:08:29'),
(175,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 16:08:56'),
(176,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 16:09:40'),
(177,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 16:12:27'),
(178,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 16:13:21'),
(179,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-21 16:13:36'),
(180,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-22 13:18:57'),
(181,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-22 14:34:26'),
(182,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-24 01:24:00'),
(183,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-01-25 20:06:34'),
(184,'127.0.0.1',1,'sximo','gallery','New Entry row with ID : 1  , Has Been Save Successfull','2015-01-26 11:12:44'),
(185,'127.0.0.1',1,'sximo','gallery','New Entry row with ID : 1  , Has Been Save Successfull','2015-01-26 12:42:16'),
(186,'127.0.0.1',1,'sximo','gallery','New Entry row with ID : 1  , Has Been Save Successfull','2015-01-26 16:00:06'),
(187,'127.0.0.1',1,'sximo','faq','New Entry row with ID : 1  , Has Been Save Successfull','2015-01-28 08:11:58'),
(188,'127.0.0.1',1,'sximo','faq','New Entry row with ID : 1  , Has Been Save Successfull','2015-01-28 08:25:28'),
(189,'127.0.0.1',1,'sximo','faq','New Entry row with ID : 1  , Has Been Save Successfull','2015-01-28 08:25:50'),
(190,'127.0.0.1',1,'sximo','faq','New Entry row with ID : 1  , Has Been Save Successfull','2015-01-28 08:26:55'),
(191,'127.0.0.1',1,'sximo','blogcat','New Entry row with ID : 2  , Has Been Save Successfull','2015-01-29 12:17:37'),
(192,'127.0.0.1',1,'sximo','blogcat','New Entry row with ID : 7  , Has Been Save Successfull','2015-01-29 12:28:53'),
(193,'127.0.0.1',1,'sximo','blogcat','ID : 7  , Has Been Removed Successfull','2015-01-29 12:29:04'),
(194,'127.0.0.1',1,'sximo','blog','New Entry row with ID : 1  , Has Been Save Successfull','2015-01-29 18:43:56'),
(195,'127.0.0.1',1,'sximo','blog','New Entry row with ID : 1  , Has Been Save Successfull','2015-01-30 14:44:31'),
(196,'127.0.0.1',1,'blog','savecomment','New Comment with ID :   , Has Been Save Successfull','2015-02-02 09:53:02'),
(197,'127.0.0.1',1,'blog','savecomment','New Comment with ID :   , Has Been Save Successfull','2015-02-02 09:53:56'),
(198,'127.0.0.1',1,'sximo','blog','New Entry row with ID : 2  , Has Been Save Successfull','2015-02-02 10:10:57'),
(199,'127.0.0.1',1,'sximo','blog','New Entry row with ID : 2  , Has Been Save Successfull','2015-02-02 10:11:04'),
(200,'127.0.0.1',1,'blog','savecomment','New Comment with ID :   , Has Been Save Successfull','2015-02-02 10:14:12'),
(201,'127.0.0.1',1,'blog','savecomment','New Comment with ID :   , Has Been Save Successfull','2015-02-02 10:15:22'),
(202,'127.0.0.1',1,'sximo','blogcomment','ID : 15,16,17  , Has Been Removed Successfull','2015-02-02 10:16:31'),
(203,'127.0.0.1',1,'blog','savecomment','New Comment with ID :   , Has Been Save Successfull','2015-02-02 11:54:13'),
(204,'127.0.0.1',1,'sximo','blogcomment','ID : 18  , Has Been Removed Successfull','2015-02-02 11:58:52'),
(205,'127.0.0.1',1,'sximo','faq','New Entry row with ID : 1  , Has Been Save Successfull','2015-02-02 14:32:40'),
(206,'127.0.0.1',1,'sximo','blog','New Entry row with ID : 1  , Has Been Save Successfull','2015-02-02 14:33:53'),
(207,'127.0.0.1',1,'sximo','blog','New Entry row with ID : 1  , Has Been Save Successfull','2015-02-02 14:51:19'),
(208,'127.0.0.1',1,'sximo','blog','New Entry row with ID : 1  , Has Been Save Successfull','2015-02-02 14:53:05'),
(209,'127.0.0.1',1,'sximo','faq','New Entry row with ID : 1  , Has Been Save Successfull','2015-02-04 14:43:48'),
(210,'127.0.0.1',1,'sximo','faq','New Entry row with ID : 1  , Has Been Save Successfull','2015-02-04 15:33:45'),
(211,'127.0.0.1',1,'sximo','faq','New Entry row with ID : 1  , Has Been Save Successfull','2015-02-04 15:38:28'),
(212,'127.0.0.1',1,'sximo','faq','New Entry row with ID : 2  , Has Been Save Successfull','2015-02-04 15:44:41'),
(213,'127.0.0.1',1,'sximo','megamenu','New Entry row with ID : 12  , Has Been Save Successfull','2015-02-10 08:19:04'),
(214,'127.0.0.1',1,'sximo','blog','New Entry row with ID : 2  , Has Been Save Successfull','2015-02-13 07:26:22'),
(215,'127.0.0.1',1,'blog','saveComment','New Entry row with ID : 14  , Has Been Save Successfull','2015-04-26 13:30:58'),
(216,'127.0.0.1',1,'blog','saveComment','New Entry row with ID : 15  , Has Been Save Successfull','2015-04-26 13:33:23'),
(217,'127.0.0.1',1,'users','save','New Entry row with ID :   , Has Been Save Successfull','2015-05-19 00:30:49'),
(218,'127.0.0.1',1,'employee','save','New Entry row with ID : 1  , Has Been Save Successfull','2015-05-19 01:00:18'),
(219,'127.0.0.1',1,'employee','save','New Entry row with ID : 9  , Has Been Save Successfull','2015-05-19 01:17:36'),
(220,'127.0.0.1',1,'employee','destroy','ID : 2  , Has Been Removed Successfull','2015-05-19 01:19:52'),
(221,'127.0.0.1',1,'employee','destroy','ID : 6,7,8  , Has Been Removed Successfull','2015-05-19 01:20:52'),
(222,'127.0.0.1',1,'blog','saveComment','New Entry row with ID : 16  , Has Been Save Successfull','2015-05-19 01:21:47'),
(223,'127.0.0.1',1,'users','save','New Entry row with ID :   , Has Been Save Successfull','2015-05-19 01:50:59'),
(224,'127.0.0.1',1,'users','save','New Entry row with ID :   , Has Been Save Successfull','2015-05-19 01:51:19'),
(225,'127.0.0.1',1,'users','save','New Entry row with ID :   , Has Been Save Successfull','2015-05-19 01:51:38'),
(226,'127.0.0.1',1,'groups','save','New Entry row with ID : 4  , Has Been Save Successfull','2015-05-19 01:53:14'),
(227,'127.0.0.1',1,'groups','save','New Entry row with ID : 5  , Has Been Save Successfull','2015-05-19 01:54:22'),
(228,'127.0.0.1',1,'groups','destroy','ID : 4,5  , Has Been Removed Successfull','2015-05-19 01:55:11'),
(229,'127.0.0.1',1,'groups','save','New Entry row with ID : 6  , Has Been Save Successfull','2015-05-19 01:55:32'),
(230,'127.0.0.1',1,'groups','destroy','ID : 6  , Has Been Removed Successfull','2015-05-19 01:58:20'),
(231,'127.0.0.1',1,'groups','save','New Entry row with ID : 7  , Has Been Save Successfull','2015-05-19 01:58:57'),
(232,'127.0.0.1',1,'groups','save','New Entry row with ID : 8  , Has Been Save Successfull','2015-05-19 02:02:13'),
(233,'127.0.0.1',1,'groups','destroy','ID : 7,8  , Has Been Removed Successfull','2015-05-19 02:02:40'),
(234,'127.0.0.1',1,'groups','save','New Entry row with ID : 9  , Has Been Save Successfull','2015-05-19 02:10:48'),
(235,'127.0.0.1',1,'groups','save','New Entry row with ID : 10  , Has Been Save Successfull','2015-05-19 02:12:12'),
(236,'127.0.0.1',1,'groups','destroy','ID : 9,10  , Has Been Removed Successfull','2015-05-19 02:40:33'),
(237,'127.0.0.1',1,'customer','destroy','ID : 72  , Has Been Removed Successfull','2015-05-19 20:05:54'),
(238,'127.0.0.1',1,'customer','save','New Entry row with ID : 2  , Has Been Save Successfull','2015-05-19 20:07:54'),
(239,'127.0.0.1',1,'customer','save','New Entry row with ID : 3  , Has Been Save Successfull','2015-05-19 20:20:04'),
(240,'127.0.0.1',1,'customer','save','New Entry row with ID : 2  , Has Been Save Successfull','2015-05-19 20:22:04'),
(241,'127.0.0.1',1,'groups','save','New Entry row with ID : 4  , Has Been Save Successfull','2015-05-22 01:23:11'),
(242,'127.0.0.1',1,'employee','save','New Entry row with ID : 1  , Has Been Save Successfull','2015-05-22 01:28:04'),
(243,'127.0.0.1',1,'employee','save','New Entry row with ID : 1  , Has Been Save Successfull','2015-05-22 01:29:24'),
(244,'127.0.0.1',1,'employee','save','New Entry row with ID : 1  , Has Been Save Successfull','2015-06-25 23:52:20'),
(245,'127.0.0.1',1,'customer','save','New Entry row with ID : 2  , Has Been Save Successfull','2015-07-06 20:43:50'),
(246,'127.0.0.1',1,'users','destroy','ID : 3,4  , Has Been Removed Successfull','2015-07-06 20:57:48'),
(247,'127.0.0.1',1,'groups','destroy','ID : 4  , Has Been Removed Successfull','2015-07-06 20:58:26'),
(248,'127.0.0.1',1,'blog','saveComment','New Entry row with ID : 17  , Has Been Save Successfull','2015-07-16 15:00:38'),
(249,'127.0.0.1',1,'blog','saveComment','New Entry row with ID : 18  , Has Been Save Successfull','2015-07-16 15:15:15'),
(250,'::1',1,'perangkat','save','New Entry row with ID : K520J01767  , Has Been Save Successfull','2019-07-24 09:42:50'),
(251,'::1',1,'perangkat','save','New Entry row with ID : K520J01767  , Has Been Save Successfull','2019-07-24 09:43:23'),
(252,'::1',1,'perangkat','save','New Entry row with ID : K520J01767  , Has Been Save Successfull','2019-07-24 09:45:02'),
(253,'::1',1,'presensi','save','New Entry row with ID : 5  , Has Been Save Successfull','2019-07-24 09:50:42'),
(254,'::1',1,'perangkat','save','New Entry row with ID : K520J01767  , Has Been Save Successfull','2019-07-24 09:52:12'),
(255,'::1',1,'perangkat','save','New Entry row with ID : K520J01767  , Has Been Save Successfull','2019-07-24 09:54:32'),
(256,'::1',1,'perangkat','save','New Entry row with ID : 0  , Has Been Save Successfull','2019-07-24 09:56:11'),
(257,'::1',1,'presensi','destroy','ID : 5  , Has Been Removed Successfull','2019-07-24 09:58:02'),
(258,'::1',1,'perangkat','save','New Entry row with ID : 2  , Has Been Save Successfull','2019-07-30 15:50:27'),
(259,'::1',1,'perangkat','save','New Entry row with ID : 3  , Has Been Save Successfull','2019-07-30 15:51:47'),
(260,'::1',1,'perangkat','destroy','ID : 3  , Has Been Removed Successfull','2019-07-30 15:52:40');

/*Table structure for table `tb_megamenu` */

DROP TABLE IF EXISTS `tb_megamenu`;

CREATE TABLE `tb_megamenu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_enable` int(2) DEFAULT NULL,
  `menu_type` text,
  `columns_count` int(2) DEFAULT NULL,
  `link_rows` int(5) DEFAULT NULL,
  `submenu_option` text,
  `content_html` text,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `tb_megamenu` */

insert  into `tb_megamenu`(`menu_id`,`is_enable`,`menu_type`,`columns_count`,`link_rows`,`submenu_option`,`content_html`) values 
(12,0,'submenu',2,7,'a:7:{s:11:\"is_internal\";a:4:{i:0;a:20:{i:0;s:1:\"1\";i:1;s:0:\"\";i:2;s:0:\"\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:0:\"\";i:6;s:0:\"\";i:7;s:0:\"\";i:8;s:0:\"\";i:9;s:0:\"\";i:10;s:0:\"\";i:11;s:0:\"\";i:12;s:0:\"\";i:13;s:0:\"\";i:14;s:0:\"\";i:15;s:0:\"\";i:16;s:0:\"\";i:17;s:0:\"\";i:18;s:0:\"\";i:19;s:0:\"\";}i:1;a:20:{i:0;s:0:\"\";i:1;s:0:\"\";i:2;s:0:\"\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:0:\"\";i:6;s:0:\"\";i:7;s:0:\"\";i:8;s:0:\"\";i:9;s:0:\"\";i:10;s:0:\"\";i:11;s:0:\"\";i:12;s:0:\"\";i:13;s:0:\"\";i:14;s:0:\"\";i:15;s:0:\"\";i:16;s:0:\"\";i:17;s:0:\"\";i:18;s:0:\"\";i:19;s:0:\"\";}i:2;a:20:{i:0;s:0:\"\";i:1;s:0:\"\";i:2;s:0:\"\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:0:\"\";i:6;s:0:\"\";i:7;s:0:\"\";i:8;s:0:\"\";i:9;s:0:\"\";i:10;s:0:\"\";i:11;s:0:\"\";i:12;s:0:\"\";i:13;s:0:\"\";i:14;s:0:\"\";i:15;s:0:\"\";i:16;s:0:\"\";i:17;s:0:\"\";i:18;s:0:\"\";i:19;s:0:\"\";}i:3;a:20:{i:0;s:0:\"\";i:1;s:0:\"\";i:2;s:0:\"\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:0:\"\";i:6;s:0:\"\";i:7;s:0:\"\";i:8;s:0:\"\";i:9;s:0:\"\";i:10;s:0:\"\";i:11;s:0:\"\";i:12;s:0:\"\";i:13;s:0:\"\";i:14;s:0:\"\";i:15;s:0:\"\";i:16;s:0:\"\";i:17;s:0:\"\";i:18;s:0:\"\";i:19;s:0:\"\";}}s:6:\"header\";a:2:{i:0;s:6:\"Solois\";i:1;s:5:\"joglo\";}s:14:\"image_file_alt\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}s:5:\"descr\";a:2:{i:0;s:187:\"Donec a lacus ac massa molestie sollicitudin eu at libero. Nulla rhoncus placerat luctus. Donec et viverra eros. Nulla facilisi. Nunc malesuada dapibus dapibus. Fusce sed pretium mauris. \";i:1;s:139:\"Ut lectus sem, mollis eget consectetur et, porta sed elit. Praesent in congue augue. Suspendisse faucibus nisl ac arcu adipiscing lobortis.\";}s:5:\"title\";a:2:{i:0;a:21:{i:0;s:2:\"ny\";i:1;s:0:\"\";i:2;s:0:\"\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:0:\"\";i:6;s:0:\"\";i:7;s:0:\"\";i:8;s:0:\"\";i:9;s:0:\"\";i:10;s:0:\"\";i:11;s:0:\"\";i:12;s:0:\"\";i:13;s:0:\"\";i:14;s:0:\"\";i:15;s:0:\"\";i:16;s:0:\"\";i:17;s:0:\"\";i:18;s:0:\"\";i:19;s:0:\"\";i:20;s:0:\"\";}i:1;a:21:{i:0;s:4:\"baru\";i:1;s:0:\"\";i:2;s:0:\"\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:0:\"\";i:6;s:0:\"\";i:7;s:0:\"\";i:8;s:0:\"\";i:9;s:0:\"\";i:10;s:0:\"\";i:11;s:0:\"\";i:12;s:0:\"\";i:13;s:0:\"\";i:14;s:0:\"\";i:15;s:0:\"\";i:16;s:0:\"\";i:17;s:0:\"\";i:18;s:0:\"\";i:19;s:0:\"\";i:20;s:0:\"\";}}s:12:\"url_external\";a:2:{i:0;a:21:{i:0;s:0:\"\";i:1;s:0:\"\";i:2;s:0:\"\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:0:\"\";i:6;s:0:\"\";i:7;s:0:\"\";i:8;s:0:\"\";i:9;s:0:\"\";i:10;s:0:\"\";i:11;s:0:\"\";i:12;s:0:\"\";i:13;s:0:\"\";i:14;s:0:\"\";i:15;s:0:\"\";i:16;s:0:\"\";i:17;s:0:\"\";i:18;s:0:\"\";i:19;s:0:\"\";i:20;s:0:\"\";}i:1;a:21:{i:0;s:0:\"\";i:1;s:0:\"\";i:2;s:0:\"\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:0:\"\";i:6;s:0:\"\";i:7;s:0:\"\";i:8;s:0:\"\";i:9;s:0:\"\";i:10;s:0:\"\";i:11;s:0:\"\";i:12;s:0:\"\";i:13;s:0:\"\";i:14;s:0:\"\";i:15;s:0:\"\";i:16;s:0:\"\";i:17;s:0:\"\";i:18;s:0:\"\";i:19;s:0:\"\";i:20;s:0:\"\";}}s:12:\"url_internal\";a:2:{i:0;a:21:{i:0;s:4:\"test\";i:1;s:4:\"home\";i:2;s:4:\"home\";i:3;s:4:\"home\";i:4;s:4:\"home\";i:5;s:4:\"home\";i:6;s:4:\"home\";i:7;s:4:\"home\";i:8;s:4:\"home\";i:9;s:4:\"home\";i:10;s:4:\"home\";i:11;s:4:\"home\";i:12;s:4:\"home\";i:13;s:4:\"home\";i:14;s:4:\"home\";i:15;s:4:\"home\";i:16;s:4:\"home\";i:17;s:4:\"home\";i:18;s:4:\"home\";i:19;s:4:\"home\";i:20;s:4:\"home\";}i:1;a:21:{i:0;s:4:\"home\";i:1;s:4:\"home\";i:2;s:4:\"home\";i:3;s:4:\"home\";i:4;s:4:\"home\";i:5;s:4:\"home\";i:6;s:4:\"home\";i:7;s:4:\"home\";i:8;s:4:\"home\";i:9;s:4:\"home\";i:10;s:4:\"home\";i:11;s:4:\"home\";i:12;s:4:\"home\";i:13;s:4:\"home\";i:14;s:4:\"home\";i:15;s:4:\"home\";i:16;s:4:\"home\";i:17;s:4:\"home\";i:18;s:4:\"home\";i:19;s:4:\"home\";i:20;s:4:\"home\";}}}','');

/*Table structure for table `tb_menu` */

DROP TABLE IF EXISTS `tb_menu`;

CREATE TABLE `tb_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `menu_name` varchar(100) DEFAULT NULL,
  `menu_type` char(10) DEFAULT NULL,
  `role_id` varchar(100) DEFAULT NULL,
  `deep` smallint(2) DEFAULT NULL,
  `ordering` int(6) DEFAULT NULL,
  `position` enum('top','sidebar','both') DEFAULT NULL,
  `menu_icons` varchar(30) DEFAULT NULL,
  `active` enum('0','1') DEFAULT '1',
  `access_data` text,
  `allow_guest` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `tb_menu` */

insert  into `tb_menu`(`menu_id`,`parent_id`,`module`,`url`,`menu_name`,`menu_type`,`role_id`,`deep`,`ordering`,`position`,`menu_icons`,`active`,`access_data`,`allow_guest`) values 
(2,0,'contact-us','','Contact Us','internal',NULL,NULL,NULL,'top','','1','{\"1\":\"0\",\"2\":\"0\",\"3\":\"0\"}','1'),
(7,0,'faq','','FAQ','internal',NULL,NULL,NULL,'top','','0','{\"1\":\"1\",\"2\":\"0\",\"3\":\"0\"}','1'),
(12,0,'about-us','','About','internal',NULL,NULL,NULL,'top','','0','{\"1\":\"1\",\"2\":\"0\",\"3\":\"0\"}','1'),
(13,0,'service','','Service','internal',NULL,NULL,NULL,'top','','0','{\"1\":\"0\",\"2\":\"0\",\"3\":\"0\"}','1'),
(18,0,'invoice','','abd','internal','0',0,0,'sidebar','','1','{\"1\":\"0\",\"2\":\"0\",\"3\":\"0\"}','0'),
(28,0,'perangkat','','Daftar Perangkat','internal',NULL,NULL,NULL,'sidebar','fa fa-desktop ','1','{\"1\":\"1\",\"2\":\"1\",\"3\":\"0\"}','0'),
(29,0,'mahasiswa','','Mahasiswa','internal',NULL,NULL,NULL,'sidebar','fa fa-graduation-cap','1','{\"1\":\"1\",\"2\":\"0\",\"3\":\"0\"}','0');

/*Table structure for table `tb_module` */

DROP TABLE IF EXISTS `tb_module`;

CREATE TABLE `tb_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(100) DEFAULT NULL,
  `module_title` varchar(100) DEFAULT NULL,
  `module_note` varchar(255) DEFAULT NULL,
  `module_author` varchar(100) DEFAULT NULL,
  `module_created` timestamp NULL DEFAULT NULL,
  `module_desc` text,
  `module_db` varchar(255) DEFAULT NULL,
  `module_db_key` varchar(100) DEFAULT NULL,
  `module_type` enum('master','report','proccess','core','generic','addon') DEFAULT 'master',
  `module_config` text,
  `module_lang` text,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

/*Data for the table `tb_module` */

insert  into `tb_module`(`module_id`,`module_name`,`module_title`,`module_note`,`module_author`,`module_created`,`module_desc`,`module_db`,`module_db_key`,`module_type`,`module_config`,`module_lang`) values 
(1,'users','Users System','Users System','Mango Tm','2013-07-10 22:46:46',NULL,'tb_users','id','core','eyJ0YWJsZV9kY4oIonR4XgVzZXJzo4w4cHJ1bWFyeV9rZXk4O4JlciVyXi3ko4w4cgFsXgN3bGVjdCoIo3NFTEVDVCA5dGJfdXN3cnMuK4w5oHR4XidybgVwcymuYWl3oFxyXGmGUk9NoHR4XgVzZXJzoExFR3Q5Sk9JT4B0Y39ncp9lcHM5T0a5dGJfZgJvdXBzLpdybgVwXi3koD05dGJfdXN3cnMuZgJvdXBf6WQ4LCJzcWxfdih3cpU4O4o5oCBXSEVSRSB0Y39lciVycym1ZCAhPScnoCA4LCJzcWxfZgJvdXA4O4o5oCA5o4w4Zp9ybXM4O3t7opZ1ZWxkoj246WQ4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JJZCosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2xLCJ0eXB3oj246G3kZGVuo4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIojE4LCJzbgJ0bG3zdCoIojA4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24ZgJvdXBf6WQ4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JHcp9lcCBcLyBMZXZ3bCosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIonJ3cXV1cpVko4w4dp33dyoIMSw4dH3wZSoIonN3bGVjdCosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4O4oyo4w4bgB06W9uoj17op9wdF90eXB3oj24ZXh0ZXJuYWw4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4J0Y39ncp9lcHM4LCJsbi9rdXBf6iVmoj24ZgJvdXBf6WQ4LCJsbi9rdXBfdpFsdWU4O4JuYWl3o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24dXN3cpmhbWU4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JVciVybpFtZSosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIonJ3cXV1cpVko4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj24Myosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4Jp6XJzdF9uYWl3o4w4YWx1YXM4O4J0Y39lciVycyosopxhYpVsoj24Rp3ycgQ5TpFtZSosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIonJ3cXV1cpVko4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj24NCosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JsYXN0XimhbWU4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JMYXN0oEmhbWU4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj24NSosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4J3bWF1bCosopFs6WFzoj24dGJfdXN3cnM4LCJsYWJ3bCoIokVtYW3so4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24ZWlh6Ww4LCJi6WVgoj2xLCJ0eXB3oj24dGVadCosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4O4oio4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonBhcgNgbgJko4w4YWx1YXM4O4J0Y39lciVycyosopxhYpVsoj24UGFzcgdvcpQ4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMCw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj2wLCJzbgJ0bG3zdCoIojc4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4owo4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIopxvZi3uXiF0dGVtcHQ4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JMbid1b4BBdHR3bXB0o4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjEsonRmcGU4O4J0ZXh0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIMCw4ci9ydGx1cgQ4O4oao4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIopxhcgRfbG9n6Wa4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JMYXN0oExvZi3uo4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjAsonRmcGU4O4J0ZXh0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIMCw4ci9ydGx1cgQ4O4omo4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24MCosopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JhYgR1dpU4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JTdGF0dXM4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4JyZXFl6XJ3ZCosonZ1ZXc4OjEsonRmcGU4O4JyYWR1byosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4O4oxMCosop9wdG3vb4oIeyJvcHRfdH3wZSoIopRhdGFs6XN0o4w4bG9v6gVwXgFlZXJmoj24MD1JbpFjdG3iZXwxOkFjdG3iZSosopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JhdpF0YXo4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JBdpF0YXo4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIopZ1bGU4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj2wLCJzbgJ0bG3zdCoIojExo4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo3wvdXBsbiFkclwvdXN3cnNcLyosonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIop3tYWd3o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24YgJ3YXR3ZF9hdCosopFs6WFzoj24dGJfdXN3cnM4LCJsYWJ3bCoIokNyZWF0ZWQ5QXQ4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMCw4dH3wZSoIonR3eHRhcpVho4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIMCw4ci9ydGx1cgQ4O4oxM4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JlcGRhdGVkXiF0o4w4YWx1YXM4O4J0Y39lciVycyosopxhYpVsoj24VXBkYXR3ZCBBdCosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2wLCJ0eXB3oj24dGVadGFyZWE4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj2wLCJzbgJ0bG3zdCoIojEzo4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fXldLCJncp3koj1beyJp6WVsZCoIop3ko4w4YWx1YXM4O4J0Y39lciVycyosopxhYpVsoj24SWQ4LCJi6WVgoj2wLCJkZXRh6Ww4OjAsonNvcnRhYpx3oj2wLCJzZWFyYi54OjEsopRvdimsbiFkoj2wLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4owo4w4Yi9ub4oIeyJiYWx1ZCoIo4osopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopFiYXRhc4osopFs6WFzoj24dGJfdXN3cnM4LCJsYWJ3bCoIokFiYXRhc4osonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjAsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4ozMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24MSosopNvbpa4Ons4dpFs6WQ4O4o4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMSw4cGF06CoIo3wvdXBsbiFkclwvdXN3cnM4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24ZgJvdXBf6WQ4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JHcp9lcCosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjAsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojM4LCJjbimuoj17onZhbG3koj24MSosopR4oj24dGJfZgJvdXBzo4w46iVmoj24ZgJvdXBf6WQ4LCJk6XNwbGFmoj24bpFtZSJ9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopmhbWU4LCJhbG3hcyoIonR4XidybgVwcyosopxhYpVsoj24RgJvdXA4LCJi6WVgoj2wLCJkZXRh6Ww4OjAsonNvcnRhYpx3oj2wLCJzZWFyYi54OjEsopRvdimsbiFkoj2wLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4o0o4w4Yi9ub4oIeyJiYWx1ZCoIo4osopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIonVzZXJuYWl3o4w4YWx1YXM4O4J0Y39lciVycyosopxhYpVsoj24VXN3cpmhbWU4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4o4LCJzbgJ0bG3zdCoIojU4LCJjbimuoj17onZhbG3koj24o4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24Zp3ycgRfbpFtZSosopFs6WFzoj24dGJfdXN3cnM4LCJsYWJ3bCoIokZ1cnN0oEmhbWU4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4o4LCJzbgJ0bG3zdCoIojY4LCJjbimuoj17onZhbG3koj24o4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24bGFzdF9uYWl3o4w4YWx1YXM4O4J0Y39lciVycyosopxhYpVsoj24TGFzdCBOYWl3o4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24o4w4ci9ydGx1cgQ4O4ogo4w4Yi9ub4oIeyJiYWx1ZCoIo4osopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopVtYW3so4w4YWx1YXM4O4J0Y39lciVycyosopxhYpVsoj24RWlh6Ww4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4o4LCJzbgJ0bG3zdCoIoj54LCJjbimuoj17onZhbG3koj24o4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24cGFzcgdvcpQ4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JQYXNzdi9yZCosonZ1ZXc4OjAsopR3dGF1bCoIMCw4ci9ydGF4bGU4OjAsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojk4LCJjbimuoj17onZhbG3koj24o4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24bG9n6WmfYXR0ZWlwdCosopFs6WFzoj24dGJfdXN3cnM4LCJsYWJ3bCoIokxvZi3uoEF0dGVtcHQ4LCJi6WVgoj2wLCJkZXRh6Ww4OjAsonNvcnRhYpx3oj2wLCJzZWFyYi54OjEsopRvdimsbiFkoj2wLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oxMCosopNvbpa4Ons4dpFs6WQ4O4o4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JjcpVhdGVkXiF0o4w4YWx1YXM4O4J0Y39lciVycyosopxhYpVsoj24QgJ3YXR3ZCBBdCosonZ1ZXc4OjAsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjAsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojExo4w4Yi9ub4oIeyJiYWx1ZCoIo4osopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopxhcgRfbG9n6Wa4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JMYXN0oExvZi3uo4w4dp33dyoIMCw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMCw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24MTo4LCJjbimuoj17onZhbG3koj24o4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24dXBkYXR3ZF9hdCosopFs6WFzoj24dGJfdXN3cnM4LCJsYWJ3bCoIo3VwZGF0ZWQ5QXQ4LCJi6WVgoj2wLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oxMyosopNvbpa4Ons4dpFs6WQ4O4o4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JhYgR1dpU4LCJhbG3hcyoIonR4XgVzZXJzo4w4bGF4ZWw4O4JBYgR1dpU4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2wLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4o4LCJzbgJ0bG3zdCoIojE0o4w4Yi9ub4oIeyJiYWx1ZCoIo4osopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fXldfQ==',NULL),
(2,'groups','Users Group','View All','Mango Tm','2013-07-10 13:45:14',NULL,'tb_groups','group_id','core','eyJ0YWJsZV9kY4oIonR4XidybgVwcyosonBy6Wlhcn3f6iVmoj246WQ4LCJzcWxfciVsZWN0oj24U0VMRUNUoCBcb3x0dGJfZgJvdXBzLpdybgVwXi3kLFxuXHR0Y39ncp9lcHMubpFtZSxcb3x0dGJfZgJvdXBzLpR3ciNy6XB06W9uLFxuXHR0Y39ncp9lcHMubGViZWxcb3xuXGmGUk9NoHR4XidybgVwcyA4LCJzcWxfdih3cpU4O4o5oFdoRVJFoHR4XidybgVwcymncp9lcF91ZCBJUyBOTlQ5T3VMTCA4LCJzcWxfZgJvdXA4O4o5oCosopZvcplzoj1beyJp6WVsZCoIopdybgVwXi3ko4w4YWx1YXM4O4J0Y39ncp9lcHM4LCJsYWJ3bCoIokdybgVwoE3ko4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4Zp9ybV9ncp9lcCoIo4osonRmcGU4O4J26WRkZWa4LCJhZGQ4O4oxo4w4ZWR1dCoIojE4LCJzZWFyYi54OjEsonNvcnRs6XN0oj2wLCJz6X13oj24cgBhbjEyo4w4bgB06W9uoj17op9wdF90eXB3oj1udWxsLCJsbi9rdXBfcXV3cnk4OpmlbGwsopxvbitlcF90YWJsZSoIbnVsbCw4bG9v6gVwXit3eSoIbnVsbCw4bG9v6gVwXgZhbHV3oj1udWxsLCJ1cl9kZXB3bpR3bpNmoj1udWxsLCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4OpmlbGwsonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4OpmlbGx9fSx7opZ1ZWxkoj24bpFtZSosopFs6WFzoj24dGJfZgJvdXBzo4w4bGF4ZWw4O4JOYWl3o4w4cpVxdW3yZWQ4O4JyZXFl6XJ3ZCosonZ1ZXc4OjEsopZvcplfZgJvdXA4O4o4LCJ0eXB3oj24dGVadCosopFkZCoIZpFsciUsopVk6XQ4OpZhbHN3LCJzZWFyYi54OjEsonNvcnRs6XN0oj2xLCJz6X13oj24cgBhbjEyo4w4bgB06W9uoj17op9wdF90eXB3oj1pYWxzZSw4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIZpFsciUsopxvbitlcF9iYWxlZSoIZpFsciUsop3zXiR3cGVuZGVuYgk4OpZhbHN3LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4OpZhbHN3LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj1pYWxzZXl9LHs4Zp33bGQ4O4JkZXNjcp3wdG3vb4osopFs6WFzoj24dGJfZgJvdXBzo4w4bGF4ZWw4O4JEZXNjcp3wdG3vb4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjEsopZvcplfZgJvdXA4O4o4LCJ0eXB3oj24dGVadGFyZWE4LCJhZGQ4O4o4LCJ3ZG30oj24o4w4ciVhcpN2oj2xLCJzbgJ0bG3zdCoIM4w4ci3IZSoIonNwYWaxM4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4J9fSx7opZ1ZWxkoj24bGViZWw4LCJhbG3hcyoIonR4XidybgVwcyosopxhYpVsoj24TGViZWw4LCJyZXFl6XJ3ZCoIonJ3cXV1cpVko4w4dp33dyoIMSw4Zp9ybV9ncp9lcCoIo4osonRmcGU4O4J0ZXh0XimlbWJ3c4osopFkZCoIZpFsciUsopVk6XQ4OpZhbHN3LCJzZWFyYi54OjEsonNvcnRs6XN0oj2zLCJz6X13oj24cgBhbjEyo4w4bgB06W9uoj17op9wdF90eXB3oj1pYWxzZSw4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIZpFsciUsopxvbitlcF9iYWxlZSoIZpFsciUsop3zXiR3cGVuZGVuYgk4OpZhbHN3LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4OpZhbHN3LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj1pYWxzZXl9XSw4ZgJ1ZCoIWgs4Zp33bGQ4O4Jncp9lcF91ZCosopFs6WFzoj24dGJfZgJvdXBzo4w4bGF4ZWw4O4JJRCosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojA4LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMCw4bG3u6yoIo4osonRhcpd3dCoIo39zZWxpo4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24bpFtZSosopFs6WFzoj24dGJfZgJvdXBzo4w4bGF4ZWw4O4JOYWl3o4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24o4w4ci9ydGx1cgQ4O4oxo4w4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjAsopx1bps4O4o4LCJ0YXJnZXQ4O4JfciVsZ4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopR3ciNy6XB06W9uo4w4YWx1YXM4O4J0Y39ncp9lcHM4LCJsYWJ3bCoIokR3ciNy6XB06W9uo4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24o4w4ci9ydGx1cgQ4O4oyo4w4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjAsopx1bps4O4o4LCJ0YXJnZXQ4O4JfciVsZ4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopx3dpVso4w4YWx1YXM4O4J0Y39ncp9lcHM4LCJsYWJ3bCoIokx3dpVso4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24MyosopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2wLCJs6Wmroj24o4w4dGFyZiV0oj24XgN3bGY4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9XX0=',NULL),
(4,'module','Module Management','All module applications','Mango Tm','2013-08-25 11:58:43',NULL,'tb_module','module_id','core','eyJ0YWJsZV9kY4oIonR4XilvZHVsZSosonBy6Wlhcn3f6iVmoj24bW9kdWx3Xi3ko4w4cgFsXgN3bGVjdCoIo3NFTEVDVCB0Y39tbiRlbGUubW9kdWx3Xi3kLHR4XilvZHVsZSmtbiRlbGVfbpFtZSx0Y39tbiRlbGUubW9kdWx3XgR1dGx3LHR4XilvZHVsZSmtbiRlbGVfbp90ZSx0Y39tbiRlbGUubW9kdWx3XiFldGhvc4x0Y39tbiRlbGUubW9kdWx3XiNyZWF0ZWQsdGJfbW9kdWx3LplvZHVsZV9kZXNjLHR4XilvZHVsZSmtbiRlbGVfZGosdGJfbW9kdWx3LplvZHVsZV9kY39rZXksdGJfbW9kdWx3LplvZHVsZV90eXB3LHR4XilvZHVsZSmncp9lcF91ZCx0Y39tbiRlbGUubW9kdWx3XgBhdG55oEZST005dGJfbW9kdWx3oCosonNxbF9g6GVyZSoIo4A5oFdoRVJFoHR4XilvZHVsZSmtbiRlbGVf6WQ5SVM5Tk9UoEmVTEw5QUmEoGlvZHVsZV9uYWl3oCE9JilvZHVsZSc5oCosonNxbF9ncp9lcCoIo4A5oCA4LCJpbgJtcyoIWgs4Zp33bGQ4O4JtbiRlbGVf6WQ4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24TW9kdWx3oE3ko4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjEsonRmcGU4O4J26WRkZWa4LCJhZGQ4OpZhbHN3LCJ3ZG30oj1pYWxzZSw4ciVhcpN2oj2wLCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4O4owo4w4bgB06W9uoj17op9wdF90eXB3oj1pYWxzZSw4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIZpFsciUsopxvbitlcF9iYWxlZSoIZpFsciUsop3zXiR3cGVuZGVuYgk4OpZhbHN3LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4OpZhbHN3LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj1pYWxzZSw4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24bW9kdWx3XimhbWU4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24TW9kdWx3oEmhbWU4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMCw4dH3wZSoIonR3eHQ4LCJhZGQ4O4o4LCJ3ZG30oj24o4w4ciVhcpN2oj2xLCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjEsop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIojA4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JtbiRlbGVfdG30bGU4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24TW9kdWx3oFR1dGx3o4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MSosonZ1ZXc4OjEsonRmcGU4O4J0ZXh0o4w4YWRkoj24o4w4ZWR1dCoIo4osonN3YXJj6CoIMSw4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj2yLCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4owo4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24bW9kdWx3XimvdGU4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24TW9kdWx3oEmvdGU4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4o4LCJi6WVgoj24MSosonRmcGU4O4J0ZXh0o4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoIMyw4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24bW9kdWx3XiFldGhvc4osopFs6WFzoj24dGJfbW9kdWx3o4w4bGF4ZWw4O4JNbiRlbGU5QXV06G9yo4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjAsonRmcGU4O4J0ZXh0o4w4YWRkoj24o4w4ZWR1dCoIo4osonN3YXJj6CoIMSw4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj20LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4owo4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24bW9kdWx3XiNyZWF0ZWQ4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24TW9kdWx3oENyZWF0ZWQ4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMCw4dH3wZSoIonR3eHRfZGF0ZXR1bWU4LCJhZGQ4O4o4LCJ3ZG30oj24o4w4ciVhcpN2oj2xLCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjUsop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIojA4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JtbiRlbGVfZGVzYyosopFs6WFzoj24dGJfbW9kdWx3o4w4bGF4ZWw4O4JNbiRlbGU5RGVzYyosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2wLCJ0eXB3oj24dGVadGFyZWE4LCJhZGQ4O4o4LCJ3ZG30oj24o4w4ciVhcpN2oj2xLCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjYsop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIojA4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JtbiRlbGVfZGo4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24TW9kdWx3oER4o4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjAsonRmcGU4O4J0ZXh0o4w4YWRkoj24o4w4ZWR1dCoIo4osonN3YXJj6CoIMSw4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj2gLCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4owo4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24bW9kdWx3XiR4Xit3eSosopFs6WFzoj24dGJfbW9kdWx3o4w4bGF4ZWw4O4JNbiRlbGU5RGo5SiVmo4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjAsonRmcGU4O4J0ZXh0o4w4YWRkoj24o4w4ZWR1dCoIo4osonN3YXJj6CoIMSw4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj2aLCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4owo4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24bW9kdWx3XgRmcGU4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24TW9kdWx3oFRmcGU4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMCw4dH3wZSoIonR3eHQ4LCJhZGQ4O4o4LCJ3ZG30oj24o4w4ciVhcpN2oj2xLCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4Ojksop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIojA4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4Jncp9lcF91ZCosopFs6WFzoj24dGJfbW9kdWx3o4w4bGF4ZWw4O4JNbiRlbGU5RgJvdXA4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIonN3bGVjdCosopFkZCoIo4osopVk6XQ4O4o4LCJzZWFyYi54OjEsonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoIMTAsop9wdG3vb4oIeyJvcHRfdH3wZSoIopVadGVybpFso4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24dGJfbW9kdWx3XidybgVwcyosopxvbitlcF9rZXk4O4Jncp9lcF91ZCosopxvbitlcF9iYWxlZSoIopdybgVwXimhbWU4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIoplvZHVsZV9wYXR2o4w4YWx1YXM4O4J0Y39tbiRlbGU4LCJsYWJ3bCoIoklvZHVsZSBQYXR2o4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24o4w4dp33dyoIojE4LCJ0eXB3oj24dGVadGFyZWE4LCJhZGQ4O4oxo4w4ZWR1dCoIojE4LCJzZWFyYi54O4oxo4w4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj2xMSw4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fV0sopdy6WQ4O3t7opZ1ZWxkoj24bW9kdWx3Xi3ko4w4YWx1YXM4O4J0Y39tbiRlbGU4LCJsYWJ3bCoIoklvZHVsZSBJZCosonZ1ZXc4OjAsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojE4LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMCw4bG3u6yoIo4osonRhcpd3dCoIo39zZWxpo4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24bW9kdWx3XgBhdG54LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24QXBwcyosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojo4LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMCw4bG3u6yoIo4osonRhcpd3dCoIo39zZWxpo4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24bW9kdWx3XimhbWU4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24Qi9udHJvbGx3c4osonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojM4LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMCw4bG3u6yoIo4osonRhcpd3dCoIo39zZWxpo4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24bW9kdWx3XgR1dGx3o4w4YWx1YXM4O4J0Y39tbiRlbGU4LCJsYWJ3bCoIoklvZHVsZSBOYWl3o4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEyMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24NCosopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2wLCJs6Wmroj24o4w4dGFyZiV0oj24XgN3bGY4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JtbiRlbGVfbp90ZSosopFs6WFzoj24dGJfbW9kdWx3o4w4bGF4ZWw4O4JObgR3o4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojElMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24NSosopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2wLCJs6Wmroj24o4w4dGFyZiV0oj24XgN3bGY4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JtbiRlbGVfYXV06G9yo4w4YWx1YXM4O4J0Y39tbiRlbGU4LCJsYWJ3bCoIokFldGhvc4osonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojY4LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMCw4bG3u6yoIo4osonRhcpd3dCoIo39zZWxpo4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24bW9kdWx3XiNyZWF0ZWQ4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24QgJ3YXR3ZCosonZ1ZXc4OjAsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojc4LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMCw4bG3u6yoIo4osonRhcpd3dCoIo39zZWxpo4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24bW9kdWx3XiR3ciM4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24TW9kdWx3oER3ciM4LCJi6WVgoj2wLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oao4w4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjAsopx1bps4O4o4LCJ0YXJnZXQ4O4JfciVsZ4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIoplvZHVsZV9kY4osopFs6WFzoj24dGJfbW9kdWx3o4w4bGF4ZWw4O4JNbiRlbGU5RGo4LCJi6WVgoj2wLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4omo4w4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjAsopx1bps4O4o4LCJ0YXJnZXQ4O4JfciVsZ4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIoplvZHVsZV9kY39rZXk4LCJhbG3hcyoIonR4XilvZHVsZSosopxhYpVsoj24TW9kdWx3oER4oEt3eSosonZ1ZXc4OjAsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojEwo4w4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjAsopx1bps4O4o4LCJ0YXJnZXQ4O4JfciVsZ4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIoplvZHVsZV90eXB3o4w4YWx1YXM4O4J0Y39tbiRlbGU4LCJsYWJ3bCoIo3RmcGU4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oxMSosopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2wLCJs6Wmroj24o4w4dGFyZiV0oj24XgN3bGY4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4Jncp9lcF91ZCosopFs6WFzoj24dGJfbW9kdWx3o4w4bGF4ZWw4O4JHcp9lcCBJZCosonZ1ZXc4OjAsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojEyo4w4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjAsopx1bps4O4o4LCJ0YXJnZXQ4O4JfciVsZ4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fXldfQ==',NULL),
(7,'menu','Menu Management','Manage All Menu','Mango Tm','2014-01-06 14:50:29',NULL,'tb_menu','menu_id','core','eyJ0YWJsZV9kY4oIonR4Xil3bnU4LCJwcp3tYXJmXit3eSoIopl3bnVf6WQ4LCJzcWxfciVsZWN0oj24U0VMRUNUoHR4Xil3bnUuK4A5R3JPTSB0Y39tZWmloCosonNxbF9g6GVyZSoIo4BXSEVSRSB0Y39tZWmlLpl3bnVf6WQ5SVM5Tk9UoEmVTEw4LCJzcWxfZgJvdXA4O4o4LCJncp3koj1beyJp6WVsZCoIopl3bnVf6WQ4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIokl3bnU5SWQ4LCJi6WVgoj24MCosopR3dGF1bCoIojA4LCJzbgJ0YWJsZSoIojE4LCJzZWFyYi54O4oxo4w4ZG9gbpxvYWQ4O4oxo4w4ZnJvepVuoj24MCosoph1ZGR3b4oIojA4LCJhbG3nb4oIopx3ZnQ4LCJg6WR06CoIojEwMCosonNvcnRs6XN0oj24MCosopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj1pYWxzZSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj1pYWxzZSw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fX0sonRmcGU4O4J0ZXh0on0seyJp6WVsZCoIonBhcpVudF91ZCosopFs6WFzoj24dGJfbWVudSosopxhYpVsoj24UGFyZWm0oE3ko4w4dp33dyoIojE4LCJkZXRh6Ww4O4oxo4w4ci9ydGF4bGU4O4oxo4w4ciVhcpN2oj24MSosopRvdimsbiFkoj24MSosopZybg13b4oIojA4LCJ26WRkZWa4O4oxo4w4YWx1Zia4O4JsZWZ0o4w4di3kdG54O4oxMDA4LCJzbgJ0bG3zdCoIojE4LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIZpFsciUsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIZpFsciUsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9LCJ0eXB3oj24dGVadCJ9LHs4Zp33bGQ4O4JtbiRlbGU4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIoklvZHVsZSosonZ1ZXc4O4oxo4w4ZGV0YW3soj24MSosonNvcnRhYpx3oj24MSosonN3YXJj6CoIojE4LCJkbgdubG9hZCoIojE4LCJpcp9IZWa4O4owo4w46G3kZGVuoj24MCosopFs6Wduoj24bGVpdCosond1ZHR2oj24MTUwo4w4ci9ydGx1cgQ4O4ozo4w4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OpZhbHN3LCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OpZhbHN3LCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fSw4dH3wZSoIonR3eHQ4fSx7opZ1ZWxkoj24dXJso4w4YWx1YXM4O4J0Y39tZWmlo4w4bGF4ZWw4O4JVcpw4LCJi6WVgoj24MCosopR3dGF1bCoIojA4LCJzbgJ0YWJsZSoIojE4LCJzZWFyYi54O4oxo4w4ZG9gbpxvYWQ4O4oxo4w4ZnJvepVuoj24MCosoph1ZGR3b4oIojA4LCJhbG3nb4oIopx3ZnQ4LCJg6WR06CoIojEwMCosonNvcnRs6XN0oj24MyosopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj1pYWxzZSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj1pYWxzZSw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fX0sonRmcGU4O4J0ZXh0on0seyJp6WVsZCoIopl3bnVfbpFtZSosopFs6WFzoj24dGJfbWVudSosopxhYpVsoj24TWVudSBOYWl3o4w4dp33dyoIojE4LCJkZXRh6Ww4O4oxo4w4ci9ydGF4bGU4O4oxo4w4ciVhcpN2oj24MSosopRvdimsbiFkoj24MSosopZybg13b4oIojA4LCJ26WRkZWa4O4owo4w4YWx1Zia4O4JsZWZ0o4w4di3kdG54O4ozMDA4LCJzbgJ0bG3zdCoIojo4LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIZpFsciUsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIZpFsciUsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9LCJ0eXB3oj24dGVadCJ9LHs4Zp33bGQ4O4JtZWmlXgRmcGU4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIokl3bnU5VH3wZSosonZ1ZXc4O4owo4w4ZGV0YW3soj24MCosonNvcnRhYpx3oj24MSosonN3YXJj6CoIojE4LCJkbgdubG9hZCoIojE4LCJpcp9IZWa4O4owo4w46G3kZGVuoj24MCosopFs6Wduoj24bGVpdCosond1ZHR2oj24MTAwo4w4ci9ydGx1cgQ4O4olo4w4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OpZhbHN3LCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OpZhbHN3LCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fSw4dH3wZSoIonR3eHQ4fSx7opZ1ZWxkoj24cp9sZV91ZCosopFs6WFzoj24dGJfbWVudSosopxhYpVsoj24Up9sZSBJZCosonZ1ZXc4O4owo4w4ZGV0YW3soj24MSosonNvcnRhYpx3oj24MSosonN3YXJj6CoIojE4LCJkbgdubG9hZCoIojE4LCJpcp9IZWa4O4owo4w46G3kZGVuoj24MCosopFs6Wduoj24bGVpdCosond1ZHR2oj24MTAwo4w4ci9ydGx1cgQ4O4oio4w4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OpZhbHN3LCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OpZhbHN3LCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fSw4dH3wZSoIonR3eHQ4fSx7opZ1ZWxkoj24ZGV3cCosopFs6WFzoj24dGJfbWVudSosopxhYpVsoj24RGV3cCosonZ1ZXc4O4owo4w4ZGV0YW3soj24MSosonNvcnRhYpx3oj24MSosonN3YXJj6CoIojE4LCJkbgdubG9hZCoIojE4LCJpcp9IZWa4O4owo4w46G3kZGVuoj24MCosopFs6Wduoj24bGVpdCosond1ZHR2oj24MTAwo4w4ci9ydGx1cgQ4O4ogo4w4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OpZhbHN3LCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OpZhbHN3LCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fSw4dH3wZSoIonR3eHQ4fSx7opZ1ZWxkoj24bgJkZXJ1bpc4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIok9yZCosonZ1ZXc4O4oxo4w4ZGV0YW3soj24MSosonNvcnRhYpx3oj24MSosonN3YXJj6CoIojE4LCJkbgdubG9hZCoIojE4LCJpcp9IZWa4O4owo4w46G3kZGVuoj24MCosopFs6Wduoj24bGVpdCosond1ZHR2oj24NTA4LCJzbgJ0bG3zdCoIoj54LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIZpFsciUsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIZpFsciUsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9LCJ0eXB3oj24dGVadCJ9LHs4Zp33bGQ4O4JwbgN1dG3vb4osopFs6WFzoj24dGJfbWVudSosopxhYpVsoj24UG9z6XR1bia4LCJi6WVgoj24MCosopR3dGF1bCoIojA4LCJzbgJ0YWJsZSoIojE4LCJzZWFyYi54O4oxo4w4ZG9gbpxvYWQ4O4oxo4w4ZnJvepVuoj24MCosoph1ZGR3b4oIojA4LCJhbG3nb4oIopx3ZnQ4LCJg6WR06CoIojEwMCosonNvcnRs6XN0oj24OSosopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj1pYWxzZSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj1pYWxzZSw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fX0sonRmcGU4O4J0ZXh0on0seyJp6WVsZCoIopl3bnVf6WNvbnM4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIo4BJYi9uo4w4dp33dyoIojE4LCJkZXRh6Ww4O4oxo4w4ci9ydGF4bGU4O4oxo4w4ciVhcpN2oj24MSosopRvdimsbiFkoj24MSosopZybg13b4oIojA4LCJ26WRkZWa4O4owo4w4YWx1Zia4O4JsZWZ0o4w4di3kdG54O4olMCosonNvcnRs6XN0oj24MTA4LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIZpFsciUsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIZpFsciUsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9LCJ0eXB3oj24dGVadCJ9LHs4Zp33bGQ4O4JhYgR1dpU4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIokFjdG3iZSosonZ1ZXc4O4oxo4w4ZGV0YW3soj24MSosonNvcnRhYpx3oj24MSosonN3YXJj6CoIojE4LCJkbgdubG9hZCoIojE4LCJpcp9IZWa4O4owo4w46G3kZGVuoj24MCosond1ZHR2oj24NTA4LCJhbG3nb4oIopN3bnR3c4osonNvcnRs6XN0oj24NyosopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj1pYWxzZSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj1pYWxzZSw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fX0sonRmcGU4O4J0ZXh0onldLCJpbgJtcyoIWgs4Zp33bGQ4O4JtZWmlXi3ko4w4YWx1YXM4O4J0Y39tZWmlo4w4bGF4ZWw4O4JNZWmloE3ko4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIojE4LCJ0eXB3oj24dGVadCosopFkZCoIojE4LCJ3ZG30oj24MSosonN3YXJj6CoIojE4LCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjAsopZvcplfZgJvdXA4O4o4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JwYXJ3bnRf6WQ4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIo3BhcpVudCBJZCosonJ3cXV1cpVkoj24MCosonZ1ZXc4O4oxo4w4dH3wZSoIonR3eHQ4LCJhZGQ4O4oxo4w4ZWR1dCoIojE4LCJzZWFyYi54O4oxo4w4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj2xLCJpbgJtXidybgVwoj24o4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24bW9kdWx3o4w4YWx1YXM4O4J0Y39tZWmlo4w4bGF4ZWw4O4JNbiRlbGU4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj24MSosonRmcGU4O4J0ZXh0o4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoIM4w4Zp9ybV9ncp9lcCoIo4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonVybCosopFs6WFzoj24dGJfbWVudSosopxhYpVsoj24VXJso4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIojE4LCJ0eXB3oj24dGVadCosopFkZCoIojE4LCJ3ZG30oj24MSosonN3YXJj6CoIojE4LCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjMsopZvcplfZgJvdXA4O4o4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JtZWmlXimhbWU4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIokl3bnU5TpFtZSosonJ3cXV1cpVkoj24MCosonZ1ZXc4O4oxo4w4dH3wZSoIonR3eHQ4LCJhZGQ4O4oxo4w4ZWR1dCoIojE4LCJzZWFyYi54O4oxo4w4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj20LCJpbgJtXidybgVwoj24o4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24bWVudV90eXB3o4w4YWx1YXM4O4J0Y39tZWmlo4w4bGF4ZWw4O4JNZWmloFRmcGU4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj24MSosonRmcGU4O4J0ZXh0o4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoINSw4Zp9ybV9ncp9lcCoIo4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonJvbGVf6WQ4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIo3JvbGU5SWQ4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj24MSosonRmcGU4O4J0ZXh0o4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoIN4w4Zp9ybV9ncp9lcCoIo4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIopR3ZXA4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIokR3ZXA4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj24MSosonRmcGU4O4J0ZXh0o4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoINyw4Zp9ybV9ncp9lcCoIo4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIop9yZGVy6Wmno4w4YWx1YXM4O4J0Y39tZWmlo4w4bGF4ZWw4O4JPcpR3cp3uZyosonJ3cXV1cpVkoj24MCosonZ1ZXc4O4oxo4w4dH3wZSoIonR3eHQ4LCJhZGQ4O4oxo4w4ZWR1dCoIojE4LCJzZWFyYi54O4oxo4w4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj2aLCJpbgJtXidybgVwoj24o4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24cG9z6XR1bia4LCJhbG3hcyoIonR4Xil3bnU4LCJsYWJ3bCoIo3Bvci306W9uo4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIojE4LCJ0eXB3oj24dGVadCosopFkZCoIojE4LCJ3ZG30oj24MSosonN3YXJj6CoIojE4LCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjksopZvcplfZgJvdXA4O4o4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JtZWmlXi3jbimzo4w4YWx1YXM4O4J0Y39tZWmlo4w4bGF4ZWw4O4JNZWmloE3jbimzo4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIojE4LCJ0eXB3oj24dGVadCosopFkZCoIojE4LCJ3ZG30oj24MSosonN3YXJj6CoIojE4LCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjEwLCJpbgJtXidybgVwoj24o4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24YWN06XZ3o4w4YWx1YXM4O4J0Y39tZWmlo4w4bGF4ZWw4O4JBYgR1dpU4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj24MSosonRmcGU4O4J0ZXh0o4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoIMTEsopZvcplfZgJvdXA4O4o4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9XX0=',NULL),
(8,'pages','Pages CMS Management','View all static pages','Mango Tm','2014-03-26 05:33:41',NULL,'tb_pages','pageID','core','eyJ0YWJsZV9kY4oIonR4XgBhZiVzo4w4cHJ1bWFyeV9rZXk4O4JwYWd3SUQ4LCJzcWxfciVsZWN0oj24oFNFTEVDVCB0Y39wYWd3cyaqoEZST005dGJfcGFnZXM5o4w4cgFsXgd2ZXJ3oj24oFdoRVJFoHR4XgBhZiVzLnBhZiVJRCBJUyBOTlQ5T3VMTCosonNxbF9ncp9lcCoIo4osopZvcplzoj1beyJp6WVsZCoIonBhZiVJRCosopFs6WFzoj24dGJfcGFnZXM4LCJsYWJ3bCoIo3BhZiVJRCosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2xLCJ0eXB3oj246G3kZGVuo4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIojE4LCJzbgJ0bG3zdCoIojE4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24dG30bGU4LCJhbG3hcyoIonR4XgBhZiVzo4w4bGF4ZWw4O4JU6XRsZSosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIonJ3cXV1cpVko4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj24M4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JubgR3o4w4YWx1YXM4O4J0Y39wYWd3cyosopxhYpVsoj24Tp90ZSosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2wLCJ0eXB3oj24dGVadCosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54OjAsonNvcnRs6XN0oj24OSosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JhbG3hcyosopFs6WFzoj24dGJfcGFnZXM4LCJsYWJ3bCoIokFs6WFzo4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24YWxwYSosonZ1ZXc4OjEsonRmcGU4O4J0ZXh0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIojE4LCJzbgJ0bG3zdCoIojM4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24YgJ3YXR3ZCosopFs6WFzoj24dGJfcGFnZXM4LCJsYWJ3bCoIokNyZWF0ZWQ4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIoph1ZGR3b4osopFkZCoIMSw4ZWR1dCoIMSw4ciVhcpN2oj24MSosonN1epU4O4o4LCJzbgJ0bG3zdCoIojc4LCJvcHR1bia4Ons4bgB0XgRmcGU4OpmlbGwsopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIbnVsbCw4bG9v6gVwXit3eSoIbnVsbCw4bG9v6gVwXgZhbHV3oj1udWxsLCJ1cl9kZXB3bpR3bpNmoj1udWxsLCJ1cl9tdWx06XBsZSoIbnVsbCw4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj1udWxsLCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj1udWxsLCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24dXBkYXR3ZCosopFs6WFzoj24dGJfcGFnZXM4LCJsYWJ3bCoIo3VwZGF0ZWQ4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIoph1ZGR3b4osopFkZCoIMSw4ZWR1dCoIMSw4ciVhcpN2oj24MSosonN1epU4O4o4LCJzbgJ0bG3zdCoIoj54LCJvcHR1bia4Ons4bgB0XgRmcGU4OpmlbGwsopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIbnVsbCw4bG9v6gVwXit3eSoIbnVsbCw4bG9v6gVwXgZhbHV3oj1udWxsLCJ1cl9kZXB3bpR3bpNmoj1udWxsLCJ1cl9tdWx06XBsZSoIbnVsbCw4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj1udWxsLCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj1udWxsLCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24Zp3sZWmhbWU4LCJhbG3hcyoIonR4XgBhZiVzo4w4bGF4ZWw4O4JG6Wx3bpFtZSosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIopFscGE4LCJi6WVgoj2xLCJ0eXB3oj24dGVadCosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4O4o0o4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonN0YXRlcyosopFs6WFzoj24dGJfcGFnZXM4LCJsYWJ3bCoIo3N0YXRlcyosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIonJ3cXV1cpVko4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj24NSosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JhYiN3cgM4LCJhbG3hcyoIonR4XgBhZiVzo4w4bGF4ZWw4O4JBYiN3cgM4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4JyZXFl6XJ3ZCosonZ1ZXc4OjEsonRmcGU4O4J0ZXh0YXJ3YSosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4O4oio4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIopFsbG9gXidlZXN0o4w4YWx1YXM4O4J0Y39wYWd3cyosopxhYpVsoj24QWxsbgc5RgV3cgQ4LCJsYWmndWFnZSoIWl0sonJ3cXV1cpVkoj24MCosonZ1ZXc4O4oxo4w4dH3wZSoIonR3eHRhcpVho4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoIOSw4Zp9ybV9ncp9lcCoIo4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonR3bXBsYXR3o4w4YWx1YXM4O4J0Y39wYWd3cyosopxhYpVsoj24VGVtcGxhdGU4LCJsYWmndWFnZSoIWl0sonJ3cXV1cpVkoj24MCosonZ1ZXc4O4oxo4w4dH3wZSoIonR3eHRhcpVho4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoIMTAsopZvcplfZgJvdXA4O4o4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9XSw4ZgJ1ZCoIWgs4Zp33bGQ4O4JwYWd3SUQ4LCJhbG3hcyoIonR4XgBhZiVzo4w4bGFuZgVhZiU4Ons46Wa4O4o4fSw4bGF4ZWw4O4JQYWd3SUQ4LCJi6WVgoj2wLCJkZXRh6Ww4OjAsonNvcnRhYpx3oj2wLCJzZWFyYi54OjEsopRvdimsbiFkoj2wLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oxo4w4Yi9ub4oIeyJiYWx1ZCoIo4osopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIonR1dGx3o4w4YWx1YXM4O4J0Y39wYWd3cyosopxhbpdlYWd3oj17op3uoj24on0sopxhYpVsoj24VG30bGU4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oyo4w4Yi9ub4oIeyJiYWx1ZCoIo4osopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopmvdGU4LCJhbG3hcyoIonR4XgBhZiVzo4w4bGFuZgVhZiU4Ons46Wa4O4o4fSw4bGF4ZWw4O4JObgR3o4w4dp33dyoIMCw4ZGV0YW3soj2wLCJzbgJ0YWJsZSoIMCw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMCw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24MyosopNvbpa4Ons4dpFs6WQ4O4o4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JhbG3hcyosopFs6WFzoj24dGJfcGFnZXM4LCJsYWmndWFnZSoIeyJ1b4oIo4J9LCJsYWJ3bCoIo3NsdWc4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4o0o4w4Yi9ub4oIeyJiYWx1ZCoIo4osopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopZ1bGVuYWl3o4w4YWx1YXM4O4J0Y39wYWd3cyosopxhbpdlYWd3oj17op3uoj24on0sopxhYpVsoj24Rp3sZWmhbWU4LCJi6WVgoj2wLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4olo4w4Yi9ub4oIeyJiYWx1ZCoIo4osopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIonN0YXRlcyosopFs6WFzoj24dGJfcGFnZXM4LCJsYWmndWFnZSoIeyJ1b4oIo4J9LCJsYWJ3bCoIo3N0YXRlcyosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojY4LCJjbimuoj17onZhbG3koj24o4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24YWNjZXNzo4w4YWx1YXM4O4J0Y39wYWd3cyosopxhbpdlYWd3oj17op3uoj24on0sopxhYpVsoj24QWNjZXNzo4w4dp33dyoIMCw4ZGV0YW3soj2wLCJzbgJ0YWJsZSoIMCw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMCw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24NyosopNvbpa4Ons4dpFs6WQ4O4o4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JjcpVhdGVko4w4YWx1YXM4O4J0Y39wYWd3cyosopxhbpdlYWd3oj17op3uoj24on0sopxhYpVsoj24QgJ3YXR3ZCosonZ1ZXc4OjAsopR3dGF1bCoIMCw4ci9ydGF4bGU4OjAsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIoj54LCJjbimuoj17onZhbG3koj24o4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24YWxsbgdfZgV3cgQ4LCJhbG3hcyoIonR4XgBhZiVzo4w4bGFuZgVhZiU4Ons46Wa4O4o4fSw4bGF4ZWw4O4JBbGxvdyBHdWVzdCosonZ1ZXc4OjAsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojk4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIonVwZGF0ZWQ4LCJhbG3hcyoIonR4XgBhZiVzo4w4bGFuZgVhZiU4Ons46Wa4O4o4fSw4bGF4ZWw4O4JVcGRhdGVko4w4dp33dyoIMCw4ZGV0YW3soj2wLCJzbgJ0YWJsZSoIMCw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMCw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24OSosopNvbpa4Ons4dpFs6WQ4O4o4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4J0ZWlwbGF0ZSosopFs6WFzoj24dGJfcGFnZXM4LCJsYWmndWFnZSoIeyJ1b4oIo4J9LCJsYWJ3bCoIo3R3bXBsYXR3o4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24MTA4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fXldfQ==',NULL),
(11,'logs','Logs','Users Activity Log','Mango Tm','2014-04-22 05:59:43',NULL,'tb_logs','auditID','core','eyJ0YWJsZV9kY4oIonR4XixvZgM4LCJwcp3tYXJmXit3eSoIopFlZG30SUQ4LCJzcWxfciVsZWN0oj24oFNFTEVDVCB0Y39sbidzL425R3JPTSB0Y39sbidzoCosonNxbF9g6GVyZSoIo4BXSEVSRSB0Y39sbidzLpFlZG30SUQ5SVM5Tk9UoEmVTEw4LCJzcWxfZgJvdXA4O4o4LCJncp3koj1beyJp6WVsZCoIopFlZG30SUQ4LCJhbG3hcyoIonR4XixvZgM4LCJsYWJ3bCoIokFlZG30SUQ4LCJi6WVgoj2wLCJkZXRh6Ww4OjAsonNvcnRhYpx3oj2wLCJzZWFyYi54OjEsopRvdimsbiFkoj2wLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oxo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4J1cGFkZHJ3cgM4LCJhbG3hcyoIonR4XixvZgM4LCJsYWJ3bCoIok3Qo4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24M4osopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24dXN3c391ZCosopFs6WFzoj24dGJfbG9ncyosopxhYpVsoj24VXN3cnM4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4ozo4w4Yi9ub4oIeyJiYWx1ZCoIojE4LCJkY4oIonR4XgVzZXJzo4w46iVmoj246WQ4LCJk6XNwbGFmoj24Zp3ycgRfbpFtZSJ9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIoplvZHVsZSosopFs6WFzoj24dGJfbG9ncyosopxhYpVsoj24TW9kdWx3o4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24NCosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24dGFz6yosopFs6WFzoj24dGJfbG9ncyosopxhYpVsoj24VGFz6yosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojU4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopmvdGU4LCJhbG3hcyoIonR4XixvZgM4LCJsYWJ3bCoIokmvdGU4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oio4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JsbidkYXR3o4w4YWx1YXM4O4J0Y39sbidzo4w4bGF4ZWw4O4JMbidkYXR3o4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24NyosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fV0sopZvcplzoj1beyJp6WVsZCoIopFlZG30SUQ4LCJhbG3hcyoIonR4XixvZgM4LCJsYWJ3bCoIokFlZG30SUQ4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj24MSosonRmcGU4O4J0ZXh0o4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoIMCw4Zp9ybV9ncp9lcCoIo4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIop3wYWRkcpVzcyosopFs6WFzoj24dGJfbG9ncyosopxhYpVsoj24SXBhZGRyZXNzo4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIojE4LCJ0eXB3oj24dGVadCosopFkZCoIojE4LCJ3ZG30oj24MSosonN3YXJj6CoIojE4LCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjEsopZvcplfZgJvdXA4O4o4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JlciVyXi3ko4w4YWx1YXM4O4J0Y39sbidzo4w4bGF4ZWw4O4JVciVyoE3ko4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjEsonRmcGU4O4JzZWx3YgQ4LCJhZGQ4OjEsopVk6XQ4OjEsonN3YXJj6CoIojE4LCJz6X13oj24o4w4ci9ydGx1cgQ4O4ozo4w4bgB06W9uoj17op9wdF90eXB3oj24ZXh0ZXJuYWw4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4J0Y39lciVycyosopxvbitlcF9rZXk4O4J1ZCosopxvbitlcF9iYWxlZSoIopZ1cnN0XimhbWU4LCJ1cl9kZXB3bpR3bpNmoj1udWxsLCJ1cl9tdWx06XBsZSoIbnVsbCw4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIbnVsbCw4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIoplvZHVsZSosopFs6WFzoj24dGJfbG9ncyosopxhYpVsoj24TW9kdWx3o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIojE4LCJ0eXB3oj24dGVadCosopFkZCoIojE4LCJ3ZG30oj24MSosonN3YXJj6CoIojE4LCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjQsopZvcplfZgJvdXA4O4o4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4J0YXNro4w4YWx1YXM4O4J0Y39sbidzo4w4bGF4ZWw4O4JUYXNro4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIojE4LCJ0eXB3oj24dGVadCosopFkZCoIojE4LCJ3ZG30oj24MSosonN3YXJj6CoIojE4LCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjUsopZvcplfZgJvdXA4O4o4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JubgR3o4w4YWx1YXM4O4J0Y39sbidzo4w4bGF4ZWw4O4JObgR3o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIojE4LCJ0eXB3oj24dGVadGFyZWE4LCJhZGQ4O4oxo4w4ZWR1dCoIojE4LCJzZWFyYi54O4oxo4w4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj2gLCJpbgJtXidybgVwoj24o4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24bG9nZGF0ZSosopFs6WFzoj24dGJfbG9ncyosopxhYpVsoj24TG9nZGF0ZSosonJ3cXV1cpVkoj24MCosonZ1ZXc4O4oxo4w4dH3wZSoIonR3eHRfZGF0ZXR1bWU4LCJhZGQ4O4oxo4w4ZWR1dCoIojE4LCJzZWFyYi54O4oxo4w4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj2aLCJpbgJtXidybgVwoj24o4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fVl9',NULL),
(45,'megamenu','Mega Menu','mega menu','Singarimbun','2015-01-16 02:55:25',NULL,'tb_megamenu','id','core','eyJ0YWJsZV9kY4oIonR4Xil3ZiFtZWmlo4w4cHJ1bWFyeV9rZXk4O4J1ZCosonNxbF9zZWx3YgQ4O4o5U0VMRUNUoHR4Xil3ZiFtZWmlL425R3JPTSB0Y39tZWdhbWVudSA4LCJzcWxfdih3cpU4O4o5V0hFUkU5dGJfbWVnYWl3bnUubWVudV91ZCBJUyBOTlQ5T3VMTCosonNxbF9ncp9lcCoIo4osopdy6WQ4O3t7opZ1ZWxkoj24bWVudV91ZCosopFs6WFzoj24dGJfbWVnYWl3bnU4LCJsYWJ3bCoIokl3bnU5SWQ4LCJsYWmndWFnZSoIWl0sonN3YXJj6CoIojE4LCJkbgdubG9hZCoIojE4LCJhbG3nb4oIopx3ZnQ4LCJi6WVgoj24MSosopR3dGF1bCoIojE4LCJzbgJ0YWJsZSoIojE4LCJpcp9IZWa4O4owo4w46G3kZGVuoj24MCosonNvcnRs6XN0oj2wLCJg6WR06CoIojEwMCosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj24MCosopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIojA4LCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIop3zXiVuYWJsZSosopFs6WFzoj24dGJfbWVnYWl3bnU4LCJsYWJ3bCoIok3zoEVuYWJsZSosopxhbpdlYWd3oj1bXSw4ciVhcpN2oj24MSosopRvdimsbiFkoj24MSosopFs6Wduoj24bGVpdCosonZ1ZXc4O4oxo4w4ZGV0YW3soj24MSosonNvcnRhYpx3oj24MSosopZybg13b4oIojA4LCJ26WRkZWa4O4owo4w4ci9ydGx1cgQ4OjEsond1ZHR2oj24MTAwo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4O4owo4w4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj24MCosonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9LCJ0eXB3oj24dGVadCJ9LHs4Zp33bGQ4O4JtZWmlXgRmcGU4LCJhbG3hcyoIonR4Xil3ZiFtZWmlo4w4bGF4ZWw4O4JNZWmloFRmcGU4LCJsYWmndWFnZSoIWl0sonN3YXJj6CoIojE4LCJkbgdubG9hZCoIojE4LCJhbG3nb4oIopx3ZnQ4LCJi6WVgoj24MSosopR3dGF1bCoIojE4LCJzbgJ0YWJsZSoIojE4LCJpcp9IZWa4O4owo4w46G3kZGVuoj24MCosonNvcnRs6XN0oj2yLCJg6WR06CoIojEwMCosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj24MCosopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIojA4LCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopNvbHVtbnNfYi9lbnQ4LCJhbG3hcyoIonR4Xil3ZiFtZWmlo4w4bGF4ZWw4O4JDbixlbWmzoENvdWm0o4w4bGFuZgVhZiU4O3tdLCJzZWFyYi54O4oxo4w4ZG9gbpxvYWQ4O4oxo4w4YWx1Zia4O4JsZWZ0o4w4dp33dyoIojE4LCJkZXRh6Ww4O4oxo4w4ci9ydGF4bGU4O4oxo4w4ZnJvepVuoj24MCosoph1ZGR3b4oIojA4LCJzbgJ0bG3zdCoIMyw4di3kdG54O4oxMDA4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIojA4LCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4O4owo4w4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4Js6WmrXgJvdgM4LCJhbG3hcyoIonR4Xil3ZiFtZWmlo4w4bGF4ZWw4O4JM6WmroFJvdgM4LCJsYWmndWFnZSoIWl0sonN3YXJj6CoIojE4LCJkbgdubG9hZCoIojE4LCJhbG3nb4oIopx3ZnQ4LCJi6WVgoj24MSosopR3dGF1bCoIojE4LCJzbgJ0YWJsZSoIojE4LCJpcp9IZWa4O4owo4w46G3kZGVuoj24MCosonNvcnRs6XN0oj20LCJg6WR06CoIojEwMCosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj24MCosopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIojA4LCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIonNlYpl3bnVfbgB06W9uo4w4YWx1YXM4O4J0Y39tZWdhbWVudSosopxhYpVsoj24UgV4bWVudSBPcHR1bia4LCJsYWmndWFnZSoIWl0sonN3YXJj6CoIojE4LCJkbgdubG9hZCoIojE4LCJhbG3nb4oIopx3ZnQ4LCJi6WVgoj24MSosopR3dGF1bCoIojE4LCJzbgJ0YWJsZSoIojE4LCJpcp9IZWa4O4owo4w46G3kZGVuoj24MCosonNvcnRs6XN0oj2lLCJg6WR06CoIojEwMCosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj24MCosopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIojA4LCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopNvbnR3bnRf6HRtbCosopFs6WFzoj24dGJfbWVnYWl3bnU4LCJsYWJ3bCoIokNvbnR3bnQ5SHRtbCosopxhbpdlYWd3oj1bXSw4ciVhcpN2oj24MSosopRvdimsbiFkoj24MSosopFs6Wduoj24bGVpdCosonZ1ZXc4O4oxo4w4ZGV0YW3soj24MSosonNvcnRhYpx3oj24MSosopZybg13b4oIojA4LCJ26WRkZWa4O4owo4w4ci9ydGx1cgQ4OjQsond1ZHR2oj24MTAwo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4O4owo4w4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj24MCosonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9LCJ0eXB3oj24dGVadCJ9XSw4Zp9ybXM4O3t7opZ1ZWxkoj24bWVudV91ZCosopFs6WFzoj24dGJfbWVnYWl3bnU4LCJsYWJ3bCoIokl3bnU5SWQ4LCJsYWmndWFnZSoIWl0sonJ3cXV1cpVkoj24MCosonZ1ZXc4O4oxo4w4dH3wZSoIonR3eHRhcpVho4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoIMCw4Zp9ybV9ncp9lcCoIo4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIop3zXiVuYWJsZSosopFs6WFzoj24dGJfbWVnYWl3bnU4LCJsYWJ3bCoIok3zoEVuYWJsZSosopxhbpdlYWd3oj1bXSw4cpVxdW3yZWQ4O4owo4w4dp33dyoIojE4LCJ0eXB3oj24dGVadCosopFkZCoIojE4LCJ3ZG30oj24MSosonN3YXJj6CoIojE4LCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjEsopZvcplfZgJvdXA4O4o4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JtZWmlXgRmcGU4LCJhbG3hcyoIonR4Xil3ZiFtZWmlo4w4bGF4ZWw4O4JNZWmloFRmcGU4LCJsYWmndWFnZSoIWl0sonJ3cXV1cpVkoj24MCosonZ1ZXc4O4oxo4w4dH3wZSoIonR3eHRhcpVho4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoIM4w4Zp9ybV9ncp9lcCoIo4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIopNvbHVtbnNfYi9lbnQ4LCJhbG3hcyoIonR4Xil3ZiFtZWmlo4w4bGF4ZWw4O4JDbixlbWmzoENvdWm0o4w4bGFuZgVhZiU4O3tdLCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj24MSosonRmcGU4O4J0ZXh0YXJ3YSosopFkZCoIojE4LCJ3ZG30oj24MSosonN3YXJj6CoIojE4LCJz6X13oj24cgBhbjEyo4w4ci9ydGx1cgQ4OjMsopZvcplfZgJvdXA4O4o4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4Js6WmrXgJvdgM4LCJhbG3hcyoIonR4Xil3ZiFtZWmlo4w4bGF4ZWw4O4JM6WmroFJvdgM4LCJsYWmndWFnZSoIWl0sonJ3cXV1cpVkoj24MCosonZ1ZXc4O4oxo4w4dH3wZSoIonR3eHRhcpVho4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoINCw4Zp9ybV9ncp9lcCoIo4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonNlYpl3bnVfbgB06W9uo4w4YWx1YXM4O4J0Y39tZWdhbWVudSosopxhYpVsoj24UgV4bWVudSBPcHR1bia4LCJsYWmndWFnZSoIWl0sonJ3cXV1cpVkoj24MCosonZ1ZXc4O4oxo4w4dH3wZSoIonR3eHRhcpVho4w4YWRkoj24MSosopVk6XQ4O4oxo4w4ciVhcpN2oj24MSosonN1epU4O4JzcGFuMTo4LCJzbgJ0bG3zdCoINSw4Zp9ybV9ncp9lcCoIo4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIopNvbnR3bnRf6HRtbCosopFs6WFzoj24dGJfbWVnYWl3bnU4LCJsYWJ3bCoIokNvbnR3bnQ5SHRtbCosopxhbpdlYWd3oj1bXSw4cpVxdW3yZWQ4O4owo4w4dp33dyoIojE4LCJ0eXB3oj24dGVadGFyZWE4LCJhZGQ4O4oxo4w4ZWR1dCoIojE4LCJzZWFyYi54O4oxo4w4ci3IZSoIonNwYWaxM4osonNvcnRs6XN0oj20LCJpbgJtXidybgVwoj24o4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fVl9',NULL),
(49,'faq','FAQ','faq','Singarimbun','2015-01-27 05:20:19',NULL,'faq','faq_id','addon','eyJ0YWJsZV9kY4oIopZhcSosonBy6Wlhcn3f6iVmoj24ZpFxXi3ko4w4Zp9ybV9jbixlbWa4OjosopZvcplfbGFmbgV0oj17opNvbHVtb4oIM4w4dG30bGU4O4JGQVEsTgB06W9uo4w4Zp9ybWF0oj24ZgJ1ZCosopR1cgBsYXk4O4JiZXJ06WNhbCJ9LCJzcWxfciVsZWN0oj24oFNFTEVDVCBpYXEuK4BGUk9NoGZhcSA4LCJzcWxfdih3cpU4O4o5V0hFUkU5ZpFxLpZhcV91ZCBJUyBOTlQ5T3VMTCosonNxbF9ncp9lcCoIo4osopZvcplzoj1beyJp6WVsZCoIopZhcV91ZCosopFs6WFzoj24ZpFxo4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIokZhcSBJZCosopZvcplfZgJvdXA4OjAsonJ3cXV1cpVkoj24MCosonZ1ZXc4OjEsonRmcGU4O4J26WRkZWa4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj2wLCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osop3zXillbHR1cGx3oj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4J0bgB1YyosopFs6WFzoj24ZpFxo4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIo3RvcG3jo4w4Zp9ybV9ncp9lcCoIMSw4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj2zLCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osop3zXillbHR1cGx3oj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JzbHVno4w4YWx1YXM4O4JpYXE4LCJsYWJ3bCoIo3NsdWc4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsopVk6XQ4OjEsonN3YXJj6CoIojE4LCJz6X13oj24o4w4ci9ydGx1cgQ4O4oyo4w4bgB06W9uoj17op9wdF90eXB3oj1pYWxzZSw4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj1pYWxzZSw4bG9v6gVwXit3eSoIZpFsciUsopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4OpZhbHN3LCJ1cl9tdWx06XBsZSoIZpFsciUsopxvbitlcF9kZXB3bpR3bpNmXit3eSoIZpFsciUsonBhdGhfdG9fdXBsbiFkoj24o4w4dXBsbiFkXgRmcGU4OpZhbHN3LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24cXV3cgR1bia4LCJhbG3hcyoIopZhcSosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JRdWVzdG3vb4osopZvcplfZgJvdXA4OjAsonJ3cXV1cpVkoj24MCosonZ1ZXc4OjEsonRmcGU4O4J0ZXh0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIojE4LCJzbgJ0bG3zdCoIMSw4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJ1cl9tdWx06XBsZSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24YWmzdiVyo4w4YWx1YXM4O4JpYXE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24QWmzdiVyo4w4Zp9ybV9ncp9lcCoIMCw4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIonR3eHRhcpVhXiVk6XRvc4osopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4Ojosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIopZ3YXRlcpVko4w4YWx1YXM4O4JpYXE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24RpVhdHVyZWQ4LCJpbgJtXidybgVwoj2xLCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2xLCJ0eXB3oj24ciVsZWN0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIojE4LCJzbgJ0bG3zdCoINCw4bgB06W9uoj17op9wdF90eXB3oj24ZGF0YWx1cgQ4LCJsbi9rdXBfcXV3cnk4O4oxO333cgwwOkmvo4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJ1cl9tdWx06XBsZSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24cHV4bG3z6GVko4w4YWx1YXM4O4JpYXE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24UHV4bG3z6GVko4w4Zp9ybV9ncp9lcCoIMSw4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIonN3bGVjdCosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4OjUsop9wdG3vb4oIeyJvcHRfdH3wZSoIopRhdGFs6XN0o4w4bG9v6gVwXgFlZXJmoj24MT1ZZXN8MD1ObyosopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fXldLCJncp3koj1beyJp6WVsZCoIopZhcV91ZCosopFs6WFzoj24ZpFxo4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIokZhcSBJZCosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojA4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIonRvcG3jo4w4YWx1YXM4O4JpYXE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24QiF0ZWdvcp33cyosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojE4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIonFlZXN06W9uo4w4YWx1YXM4O4JpYXE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24UXV3cgR1bia4LCJi6WVgoj2wLCJkZXRh6Ww4OjAsonNvcnRhYpx3oj2wLCJzZWFyYi54OjEsopRvdimsbiFkoj2wLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oyo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JzbHVno4w4YWx1YXM4O4JpYXE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24UixlZyosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojo4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopFucgd3c4osopFs6WFzoj24ZpFxo4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIokFucgd3c4osonZ1ZXc4OjAsopR3dGF1bCoIMCw4ci9ydGF4bGU4OjAsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojM4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopZ3YXRlcpVko4w4YWx1YXM4O4JpYXE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24RpVhdHVyZWQ4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4o0o4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JwdWJs6XN2ZWQ4LCJhbG3hcyoIopZhcSosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JQdWJs6XN2ZWQ4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4olo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9XX0=',NULL),
(65,'perangkat','Daftar Perangkat','Daftar Perangkat',NULL,'2019-07-24 04:55:22',NULL,'tb_perangkat','id','addon','eyJzcWxfciVsZWN0oj24oFNFTEVDVCB0Y39wZXJhbpdrYXQuK4BGUk9NoHR4XgB3cpFuZithdCA4LCJzcWxfdih3cpU4O4o5V0hFUkU5dGJfcGVyYWmn6iF0Lp3koE3ToEmPVCBOVUxMo4w4cgFsXidybgVwoj24o4w4dGF4bGVfZGo4O4J0Y39wZXJhbpdrYXQ4LCJwcp3tYXJmXit3eSoIop3ko4w4ZgJ1ZCoIWgs4Zp33bGQ4O4JkZXZ1YiVfbpFtZSosopFs6WFzoj24dGJfcGVyYWmn6iF0o4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIokR3dp3jZSBOYWl3o4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24MCosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24cia4LCJhbG3hcyoIonR4XgB3cpFuZithdCosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JTZXJ1YWw5TnVtYpVyo4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24MSosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24dpM4LCJhbG3hcyoIonR4XgB3cpFuZithdCosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JWZXJ1Zp3jYXR1bia5Qi9kZSosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojo4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopFjo4w4YWx1YXM4O4J0Y39wZXJhbpdrYXQ4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24QWN06XZhdG3vb4BDbiR3o4w4dp33dyoIMSw4ZGV0YW3soj2xLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMSw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24MyosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24dpt3eSosopFs6WFzoj24dGJfcGVyYWmn6iF0o4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIo3Z3cp3p6WNhdG3vb4BLZXk4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4o0o4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4J1ZCosopFs6WFzoj24dGJfcGVyYWmn6iF0o4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIok3ko4w4dp33dyoIMCw4ZGV0YW3soj2wLCJzbgJ0YWJsZSoIMSw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMCw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24NSosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fV0sopZvcplzoj1beyJp6WVsZCoIopR3dp3jZV9uYWl3o4w4YWx1YXM4O4J0Y39wZXJhbpdrYXQ4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24RGVi6WN3oEmhbWU4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj24MCosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonNuo4w4YWx1YXM4O4J0Y39wZXJhbpdrYXQ4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24UiVy6WFsoEmlbWJ3c4osopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2xLCJ0eXB3oj24dGVadCosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4O4oxo4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJ1cl9tdWx06XBsZSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24dpM4LCJhbG3hcyoIonR4XgB3cpFuZithdCosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JWZXJ1Zp3jYXR1bia5Qi9kZSosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2xLCJ0eXB3oj24dGVadCosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4O4oyo4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJ1cl9tdWx06XBsZSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24YWM4LCJhbG3hcyoIonR4XgB3cpFuZithdCosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JBYgR1dpF06W9uoENvZGU4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj24Myosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonZrZXk4LCJhbG3hcyoIonR4XgB3cpFuZithdCosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JWZXJ1Zp3jYXR1bia5SiVmo4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjEsonRmcGU4O4J0ZXh0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIojE4LCJzbgJ0bG3zdCoIojQ4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osop3zXillbHR1cGx3oj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4J1ZCosopFs6WFzoj24dGJfcGVyYWmn6iF0o4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIok3ko4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjEsonRmcGU4O4J26WRkZWa4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj24NSosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fXldfQ==',NULL),
(66,'mahasiswa','Data Mahasiswa','Data Mahasiswa',NULL,'2019-07-30 04:28:48',NULL,'mahasiswa','id','addon','eyJ0YWJsZV9kY4oIoplh6GFz6XNgYSosonBy6Wlhcn3f6iVmoj246WQ4LCJzcWxfciVsZWN0oj24oFNFTEVDVCBtYWhhci3zdiEuK4BGUk9NoGlh6GFz6XNgYSA4LCJzcWxfdih3cpU4O4o5V0hFUkU5bWF2YXN1cgdhLp3koE3ToEmPVCBOVUxMoGFuZCB06XB3PTA4LCJzcWxfZgJvdXA4O4o4LCJncp3koj1beyJp6WVsZCoIop3ko4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24SWQ4LCJi6WVgoj2wLCJkZXRh6Ww4OjAsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2wLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oxo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JuYWlho4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24TpFtYSosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojo4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIonR3bHBvb4osopFs6WFzoj24bWF2YXN1cgdho4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIo3R3bHBvb4osonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojM4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopm1bSosopFs6WFzoj24bWF2YXN1cgdho4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIokm1bSosonZ1ZXc4OjEsopR3dGF1bCoIMSw4ci9ydGF4bGU4OjEsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjEsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojQ4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopVtYW3so4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24RWlh6Ww4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4olo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JwYXNzdi9yZCosopFs6WFzoj24bWF2YXN1cgdho4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIo3BhcgNgbgJko4w4dp33dyoIMCw4ZGV0YW3soj2wLCJzbgJ0YWJsZSoIMCw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMCw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24N4osopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj24ZGVi6WN3o4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24RGVi6WN3o4w4dp33dyoIMCw4ZGV0YW3soj2wLCJzbgJ0YWJsZSoIMCw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMCw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24NyosopNvbpa4Ons4dpFs6WQ4O4owo4w4ZGo4O4o4LCJrZXk4O4o4LCJk6XNwbGFmoj24on0sopF0dHJ1YnV0ZSoIeyJ2eXB3cpx1bps4Ons4YWN06XZ3oj2xLCJs6Wmroj24o4w4dGFyZiV0oj24o4w46HRtbCoIo4J9LCJ1bWFnZSoIeyJhYgR1dpU4OjAsonBhdG54O4o4LCJz6X13Xg54O4o4LCJz6X13Xgk4O4o4LCJ2dGlsoj24onl9fSx7opZ1ZWxkoj246pVu6XNf6iVsYWl1b4osopFs6WFzoj24bWF2YXN1cgdho4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIok13bp3zoEt3bGFt6Wa4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4oao4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JhbGFtYXQ4LCJhbG3hcyoIoplh6GFz6XNgYSosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JBbGFtYXQ4LCJi6WVgoj2xLCJkZXRh6Ww4OjEsonNvcnRhYpx3oj2xLCJzZWFyYi54OjEsopRvdimsbiFkoj2xLCJpcp9IZWa4OjEsond1ZHR2oj24MTAwo4w4YWx1Zia4O4JsZWZ0o4w4ci9ydGx1cgQ4O4omo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4J0bit3b4osopFs6WFzoj24bWF2YXN1cgdho4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIo3Rv6iVuo4w4dp33dyoIMCw4ZGV0YW3soj2wLCJzbgJ0YWJsZSoIMCw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMCw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24MTA4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIopZvdG84LCJhbG3hcyoIoplh6GFz6XNgYSosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JGbgRvo4w4dp33dyoIMCw4ZGV0YW3soj2wLCJzbgJ0YWJsZSoIMCw4ciVhcpN2oj2xLCJkbgdubG9hZCoIMCw4ZnJvepVuoj2xLCJg6WR06CoIojEwMCosopFs6Wduoj24bGVpdCosonNvcnRs6XN0oj24MTE4LCJjbimuoj17onZhbG3koj24MCosopR4oj24o4w46iVmoj24o4w4ZG3zcGxheSoIo4J9LCJhdHRy6WJldGU4Ons46H3wZXJs6Wmroj17opFjdG3iZSoIMSw4bG3u6yoIo4osonRhcpd3dCoIo4osoph0bWw4O4o4fSw46WlhZiU4Ons4YWN06XZ3oj2wLCJwYXR2oj24o4w4ci3IZV9aoj24o4w4ci3IZV9moj24o4w46HRtbCoIo4J9fX0seyJp6WVsZCoIonN0YXRlcyosopFs6WFzoj24bWF2YXN1cgdho4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIo3N0YXRlcyosonZ1ZXc4OjAsopR3dGF1bCoIMCw4ci9ydGF4bGU4OjAsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojEyo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4J06XB3o4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24VG3wZSosonZ1ZXc4OjAsopR3dGF1bCoIMCw4ci9ydGF4bGU4OjAsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojEzo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JjcpVhdGVkXiF0o4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24QgJ3YXR3ZCBBdCosonZ1ZXc4OjAsopR3dGF1bCoIMCw4ci9ydGF4bGU4OjAsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojE0o4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9LHs4Zp33bGQ4O4JlcGRhdGVkXiF0o4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24VXBkYXR3ZCBBdCosonZ1ZXc4OjAsopR3dGF1bCoIMCw4ci9ydGF4bGU4OjAsonN3YXJj6CoIMSw4ZG9gbpxvYWQ4OjAsopZybg13b4oIMSw4di3kdG54O4oxMDA4LCJhbG3nb4oIopx3ZnQ4LCJzbgJ0bG3zdCoIojElo4w4Yi9ub4oIeyJiYWx1ZCoIojA4LCJkY4oIo4osopt3eSoIo4osopR1cgBsYXk4O4o4fSw4YXR0cp34dXR3oj17ophmcGVybG3u6yoIeyJhYgR1dpU4OjEsopx1bps4O4o4LCJ0YXJnZXQ4O4o4LCJ2dGlsoj24on0sop3tYWd3oj17opFjdG3iZSoIMCw4cGF06CoIo4osonN1epVfeCoIo4osonN1epVfeSoIo4osoph0bWw4O4o4fXl9XSw4Zp9ybXM4O3t7opZ1ZWxkoj246WQ4LCJhbG3hcyoIoplh6GFz6XNgYSosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JJZCosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2xLCJ0eXB3oj246G3kZGVuo4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIojE4LCJzbgJ0bG3zdCoIojA4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osop3zXillbHR1cGx3oj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JuYWlho4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24TpFtYSosopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2xLCJ0eXB3oj24dGVadCosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4O4oxo4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJ1cl9tdWx06XBsZSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24dGVscG9uo4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24VGVscG9uo4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjEsonRmcGU4O4J0ZXh0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIojE4LCJzbgJ0bG3zdCoIojo4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osop3zXillbHR1cGx3oj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4Ju6W04LCJhbG3hcyoIoplh6GFz6XNgYSosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JO6W04LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj24Myosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIopVtYW3so4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24RWlh6Ww4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMSw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj24MSosonNvcnRs6XN0oj24NCosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonBhcgNgbgJko4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24UGFzcgdvcpQ4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMCw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj2wLCJzbgJ0bG3zdCoIojU4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osop3zXillbHR1cGx3oj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JkZXZ1YiU4LCJhbG3hcyoIoplh6GFz6XNgYSosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JEZXZ1YiU4LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMCw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj2wLCJzbgJ0bG3zdCoIojY4LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osop3zXillbHR1cGx3oj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4JqZWm1cl9rZWxhbW3uo4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24SpVu6XM5SiVsYWl1b4osopZvcplfZgJvdXA4O4o4LCJyZXFl6XJ3ZCoIojA4LCJi6WVgoj2xLCJ0eXB3oj24dGVadCosopFkZCoIMSw4ci3IZSoIojA4LCJ3ZG30oj2xLCJzZWFyYi54O4oxo4w4ci9ydGx1cgQ4O4ogo4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJ1cl9tdWx06XBsZSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24YWxhbWF0o4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24QWxhbWF0o4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjEsonRmcGU4O4J0ZXh0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIojE4LCJzbgJ0bG3zdCoIoj54LCJvcHR1bia4Ons4bgB0XgRmcGU4O4o4LCJsbi9rdXBfcXV3cnk4O4o4LCJsbi9rdXBfdGF4bGU4O4o4LCJsbi9rdXBf6iVmoj24o4w4bG9v6gVwXgZhbHV3oj24o4w46XNfZGVwZWmkZWmjeSoIo4osop3zXillbHR1cGx3oj24o4w4bG9v6gVwXiR3cGVuZGVuYg3f6iVmoj24o4w4cGF06F90bl9lcGxvYWQ4O4o4LCJyZXN1epVfdi3kdG54O4o4LCJyZXN1epVf6GV1Zih0oj24o4w4dXBsbiFkXgRmcGU4O4o4LCJ0bi9sdG3woj24o4w4YXR0cp34dXR3oj24o4w4ZXh0ZWmkXiNsYXNzoj24onl9LHs4Zp33bGQ4O4J0bit3b4osopFs6WFzoj24bWF2YXN1cgdho4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIo3Rv6iVuo4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjAsonRmcGU4O4J0ZXh0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIMCw4ci9ydGx1cgQ4O4omo4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJ1cl9tdWx06XBsZSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24Zp90byosopFs6WFzoj24bWF2YXN1cgdho4w4bGFuZgVhZiU4O3tdLCJsYWJ3bCoIokZvdG84LCJpbgJtXidybgVwoj24o4w4cpVxdW3yZWQ4O4owo4w4dp33dyoIMCw4dH3wZSoIonR3eHQ4LCJhZGQ4OjEsonN1epU4O4owo4w4ZWR1dCoIMSw4ciVhcpN2oj2wLCJzbgJ0bG3zdCoIojEwo4w4bgB06W9uoj17op9wdF90eXB3oj24o4w4bG9v6gVwXgFlZXJmoj24o4w4bG9v6gVwXgRhYpx3oj24o4w4bG9v6gVwXit3eSoIo4osopxvbitlcF9iYWxlZSoIo4osop3zXiR3cGVuZGVuYgk4O4o4LCJ1cl9tdWx06XBsZSoIo4osopxvbitlcF9kZXB3bpR3bpNmXit3eSoIo4osonBhdGhfdG9fdXBsbiFkoj24o4w4cpVz6X13Xgd1ZHR2oj24o4w4cpVz6X13Xih36Wd2dCoIo4osonVwbG9hZF90eXB3oj24o4w4dG9vbHR1cCoIo4osopF0dHJ1YnV0ZSoIo4osopVadGVuZF9jbGFzcyoIo4J9fSx7opZ1ZWxkoj24cgRhdHVzo4w4YWx1YXM4O4JtYWhhci3zdiE4LCJsYWmndWFnZSoIWl0sopxhYpVsoj24UgRhdHVzo4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjAsonRmcGU4O4J0ZXh0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIMCw4ci9ydGx1cgQ4O4oxMSosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonR1cGU4LCJhbG3hcyoIoplh6GFz6XNgYSosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JU6XB3o4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjAsonRmcGU4O4J0ZXh0o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIMCw4ci9ydGx1cgQ4O4oxM4osop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIopNyZWF0ZWRfYXQ4LCJhbG3hcyoIoplh6GFz6XNgYSosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JDcpVhdGVkoEF0o4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjAsonRmcGU4O4J0ZXh0XiRhdGV06Wl3o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIMCw4ci9ydGx1cgQ4O4oxMyosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fX0seyJp6WVsZCoIonVwZGF0ZWRfYXQ4LCJhbG3hcyoIoplh6GFz6XNgYSosopxhbpdlYWd3oj1bXSw4bGF4ZWw4O4JVcGRhdGVkoEF0o4w4Zp9ybV9ncp9lcCoIo4osonJ3cXV1cpVkoj24MCosonZ1ZXc4OjAsonRmcGU4O4J0ZXh0XiRhdGV06Wl3o4w4YWRkoj2xLCJz6X13oj24MCosopVk6XQ4OjEsonN3YXJj6CoIMCw4ci9ydGx1cgQ4O4oxNCosop9wdG3vb4oIeyJvcHRfdH3wZSoIo4osopxvbitlcF9xdWVyeSoIo4osopxvbitlcF90YWJsZSoIo4osopxvbitlcF9rZXk4O4o4LCJsbi9rdXBfdpFsdWU4O4o4LCJ1cl9kZXB3bpR3bpNmoj24o4w46XNfbXVsdG3wbGU4O4o4LCJsbi9rdXBfZGVwZWmkZWmjeV9rZXk4O4o4LCJwYXR2XgRvXgVwbG9hZCoIo4osonJ3ci3IZV9g6WR06CoIo4osonJ3ci3IZV92ZW3n6HQ4O4o4LCJlcGxvYWRfdH3wZSoIo4osonRvbix06XA4O4o4LCJhdHRy6WJldGU4O4o4LCJ3eHR3bpRfYixhcgM4O4o4fXldfQ==',NULL);

/*Table structure for table `tb_pages` */

DROP TABLE IF EXISTS `tb_pages`;

CREATE TABLE `tb_pages` (
  `pageID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `status` enum('enable','disable') DEFAULT 'enable',
  `access` text,
  `allow_guest` enum('0','1') DEFAULT '0',
  `template` text,
  PRIMARY KEY (`pageID`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

/*Data for the table `tb_pages` */

insert  into `tb_pages`(`pageID`,`title`,`note`,`alias`,`created`,`updated`,`filename`,`status`,`access`,`allow_guest`,`template`) values 
(1,'Homepage',NULL,'home','0000-00-00 00:00:00','0000-00-00 00:00:00','home','enable','{\"1\":\"1\",\"2\":\"1\",\"3\":\"1\"}','1','frontend'),
(29,'service',NULL,'service','0000-00-00 00:00:00','0000-00-00 00:00:00','service','enable','{\"1\":\"1\",\"2\":\"0\",\"3\":\"0\"}','1','frontend'),
(27,'About Us',NULL,'about-us',NULL,NULL,'aboutus','enable','{\"1\":\"1\",\"2\":\"0\",\"3\":\"0\"}','1','frontend'),
(26,'Contact Us',NULL,'contact-us','0000-00-00 00:00:00','0000-00-00 00:00:00','contactus','enable','{\"1\":\"1\",\"2\":\"1\",\"3\":\"1\"}','1','frontend'),
(35,'FAQ',NULL,'faq','0000-00-00 00:00:00','0000-00-00 00:00:00','faq','enable','{\"1\":\"1\",\"2\":\"1\",\"3\":\"1\"}','1','frontend'),
(36,'Portpolio',NULL,'portpolio','0000-00-00 00:00:00','0000-00-00 00:00:00','portpolio','enable','{\"1\":\"1\",\"2\":\"0\",\"3\":\"0\"}','1','frontend');

/*Table structure for table `tb_perangkat` */

DROP TABLE IF EXISTS `tb_perangkat`;

CREATE TABLE `tb_perangkat` (
  `device_name` varchar(50) DEFAULT NULL,
  `sn` varchar(50) NOT NULL,
  `vc` varchar(50) DEFAULT NULL,
  `ac` varchar(50) DEFAULT NULL,
  `vkey` varchar(50) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tb_perangkat` */

insert  into `tb_perangkat`(`device_name`,`sn`,`vc`,`ac`,`vkey`,`id`) values 
('Device 1','K520J01767','B3A21015AD0BC60','6BQCC11BFF080590028333NL','AF0D853DC46ACB2A1ABD0115EC1963E5',1);

/*Table structure for table `tb_presensi` */

DROP TABLE IF EXISTS `tb_presensi`;

CREATE TABLE `tb_presensi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `data` text,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tb_presensi` */

insert  into `tb_presensi`(`id`,`user_id`,`username`,`data`,`log_time`) values 
(1,1,'','2019-07-29 08:52:20 (PC Time) | K520J01767 (SN)','2019-07-29 08:52:20'),
(2,2,'','2019-07-29 08:53:23 (PC Time) | K520J01767 (SN)','2019-07-29 08:53:23');

/*Table structure for table `tb_users` */

DROP TABLE IF EXISTS `tb_users`;

CREATE TABLE `tb_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(6) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(100) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `login_attempt` tinyint(2) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `reminder` varchar(64) DEFAULT NULL,
  `activation` varchar(50) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `finger_data` text,
  `finger_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tb_users` */

insert  into `tb_users`(`id`,`group_id`,`username`,`password`,`email`,`first_name`,`last_name`,`avatar`,`active`,`login_attempt`,`last_login`,`created_at`,`updated_at`,`reminder`,`activation`,`remember_token`,`finger_data`,`finger_id`) values 
(1,1,'superadmin','5f4dcc3b5aa765d61d8327deb882cf99','superadmin@mail.com','Root','Admin','gotik.jpg',1,12,'2014-09-09 03:09:05','2014-03-12 16:18:46','2014-12-27 14:48:15','SNLyM4Smv12Ck8jyopZJMfrypTbEDtVhGT5PMRzxs',NULL,'NGUt7iSOmpo7b13qePf1Ij76DGyBR0xDzVIibr45Yg6mOPrhC5frajgLWm6S',NULL,NULL),
(2,2,'mangopik','$2y$10$gqLJoGtIWBptZJw6qUiT2OSnfZYYIkRxoHiVlGR6j5pOzZgy8Sczu','kangopik@gmail.com','Sultans','firmansyah','foto.png',1,NULL,'2014-08-16 12:55:50','2014-04-14 04:49:59',NULL,NULL,'',NULL,NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1' COMMENT '0:superadmin;1:admin',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`level`,`remember_token`,`created_at`,`updated_at`) values 
(1,'admin','admin@gmail.com','$2y$10$/NAIn4KEIOmzxpYcX8skL.CRrHSNXB//SosViL5WSvIPXdo0wL5PC',0,'wzt9TsVIkR9NRpiI4hdyCkHDbfQPtHtbiNw6DXJ5ZY9IZ5WHZCcH1af74GLQ',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
